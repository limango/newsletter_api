package logger

import (
	"fmt"
	"time"
)

type LogType int

const (
	Success LogType = 1
	Warning         = 2
	Error           = 3
	Info            = 4
)

func Log(msg string, logType LogType) {
	fmt.Printf("%s %s: %s\n", time.Now().Format("2006-01-02 15:04:05"), getLogTypeHeader(logType), msg)
}

func getLogTypeHeader(t LogType) string {
	switch t {
	case Success:
		return "[SUCCESS]"
	case Warning:
		return "[WARNING]"
	case Error:
		return "[ERROR]"
	case Info:
		return "[INFO]"
	}
	return ""
}
