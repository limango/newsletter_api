package newsletter

import (
	"testing"
	"time"
)

type TestNewsletterValidationEntity struct {
	title             string
	newsletterInput   Newsletter
	shouldReturnError bool
}

var emptyNewsletter = Newsletter{}

var zeroIdNewsletter = Newsletter{
	VariantId:       0,
	Topic:           "some topic",
	PreheaderOption: 1,
	ShipmentDate:    time.Now(),
}

var correctNewsletter = Newsletter{
	VariantId:       123,
	Topic:           "some topic",
	PreheaderOption: 2,
	ShipmentDate:    time.Now(),
}

func TestNewsletterValidation(t *testing.T) {

	testInputs := []TestNewsletterValidationEntity{
		{
			title:             "should return error when all newsletter fields are empty",
			newsletterInput:   emptyNewsletter,
			shouldReturnError: true,
		},
		{
			title:             "should not return error when all fields are correct",
			newsletterInput:   correctNewsletter,
			shouldReturnError: false,
		},
	}

	for _, test := range testInputs {
		t.Run(test.title, func(t *testing.T) {

			err := test.newsletterInput.Validate()
			if (err != nil) != test.shouldReturnError {
				t.Errorf("\n%v \nshouldReturnError => %t \nerror => %v \nnewsletter => %v ", test.title, test.shouldReturnError, err, test.newsletterInput)
			}
		})
	}
}
