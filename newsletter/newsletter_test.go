package newsletter

import (
	"testing"
	"time"

	"bitbucket.org/limango/newsletter_api/campaign"
)

func getTime(tstr string) *time.Time {
	res, err := time.Parse(time.RFC3339, tstr)
	if err != nil {
		return nil
	}
	return &res
}

type MockCampaignProvider struct{}

func (cp *MockCampaignProvider) GetAllCampaigns() []campaign.Campaign {
	return nil
}

func (cp *MockCampaignProvider) GetCurrentCampaigns(t time.Time) []campaign.Campaign {
	return []campaign.Campaign{
		campaign.Campaign{
			Core: campaign.Core{
				Id:    1,
				Start: *getTime("2018-12-07T07:00:00+01:00"),
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Id:    2,
				Start: *getTime("2018-12-07T19:00:00+01:00"),
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Id:    3,
				Start: *getTime("2018-12-08T07:00:00+01:00"),
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Id:    4,
				Start: *getTime("2018-12-08T07:00:00+01:00"),
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Id:    5,
				Start: *getTime("2018-12-08T19:00:00+01:00"),
			},
		},
	}
}

func (cp *MockCampaignProvider) GetStartingCampaigns(t time.Time) []campaign.Campaign {
	return nil
}

func (cp *MockCampaignProvider) GetUpcomingCampaigns(t time.Time) []campaign.Campaign {
	return []campaign.Campaign{
		campaign.Campaign{
			Core: campaign.Core{
				Id:    6,
				Start: *getTime("2018-12-28T07:00:00+01:00"),
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Id:    7,
				Start: *getTime("2018-12-28T19:00:00+01:00"),
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Id:    8,
				Start: *getTime("2018-12-29T07:00:00+01:00"),
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Id:    9,
				Start: *getTime("2018-12-29T07:00:00+01:00"),
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Id:    10,
				Start: *getTime("2018-12-29T19:00:00+01:00"),
			},
		},
	}
}

func TestNewsletterGroupingCampaignsByDate(t *testing.T) {
	nt := Newsletter{}
	nt.ProvideCampaigns(&MockCampaignProvider{})

	upcoming1 := nt.Campaigns.Upcoming.GetCampaignsForDate("28.12")
	if len(upcoming1) != 2 {
		t.Errorf("newsletter incorrectly grouped campaigns by date.\n %d instead of %d returned for upcoming[2018-12-28]", len(upcoming1), 2)
	}

	upcoming2 := nt.Campaigns.Upcoming.GetCampaignsForDate("29.12")
	if len(upcoming2) != 3 {
		t.Errorf("newsletter incorrectly grouped campaigns by date.\n %d instead of %d returned for upcoming[2018-12-29]", len(upcoming2), 3)
	}

	upcoming3 := nt.Campaigns.Upcoming.GetCampaignsForDate("30.12")
	if len(upcoming3) != 0 {
		t.Errorf("newsletter incorrectly grouped campaigns by date.\n %d instead of %d returned for upcoming[2018-12-30]", len(upcoming3), 0)
	}

	upcoming4 := nt.Campaigns.Upcoming.GetCampaignsForDate("08.12")
	if len(upcoming4) != 0 {
		t.Errorf("newsletter incorrectly grouped campaigns by date.\n %d instead of %d returned for upcoming[2018-12-08] these should be in current node", len(upcoming4), 0)
	}

}

func containsCampaignWithId(campaigns []campaign.Campaign, id int) bool {
	for _, c := range campaigns {
		if c.Id == id {
			return true
		}
	}
	return false
}
