package newsletter

import (
	"testing"
	"time"

	"bitbucket.org/limango/newsletter_api/campaign"
)

var nsShipmentTime, _ = time.Parse(time.RFC3339, "2018-12-19T07:00:00+01:00")
var nsShipment7HoursBefore, _ = time.Parse(time.RFC3339, "2018-12-19T00:00:00+01:00")
var nsShipment24HoursBefore, _ = time.Parse(time.RFC3339, "2018-12-18T07:00:00+01:00")

type MockCampaignProvider1 struct{}

func (cp *MockCampaignProvider1) GetAllCampaigns() []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider1) GetUpcomingCampaigns(t time.Time) []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider1) GetCurrentCampaigns(t time.Time) []campaign.Campaign {
	return []campaign.Campaign{
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current1",
				Freetext: "Ale super1",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current2",
				Freetext: "Ale super2",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current3",
				Freetext: "Ale super3",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current4",
				Freetext: "Ale super4",
				Start:    nsShipment7HoursBefore,
			},
		},
	}
}
func (cp *MockCampaignProvider1) GetStartingCampaigns(t time.Time) []campaign.Campaign {
	return []campaign.Campaign{
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Starting1",
				Freetext: "Super freetext kampanii1",
				Start:    nsShipmentTime,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Starting2",
				Freetext: "Super freetext kampanii2",
				Start:    nsShipmentTime,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Starting3",
				Freetext: "Super freetext kampanii3",
				Start:    nsShipmentTime,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Starting4",
				Freetext: "Super freetext kampanii4",
				Start:    nsShipmentTime,
			},
		},
	}
}

type MockCampaignProvider2 struct{}

func (cp *MockCampaignProvider2) GetAllCampaigns() []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider2) GetCurrentCampaigns(t time.Time) []campaign.Campaign {
	return []campaign.Campaign{
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current1",
				Freetext: "costam",
				Start:    nsShipment7HoursBefore,
			},
		},
	}
}
func (cp *MockCampaignProvider2) GetStartingCampaigns(t time.Time) []campaign.Campaign {
	return []campaign.Campaign{
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Starting1",
				Freetext: "Ale super1",
				Start:    nsShipmentTime,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Starting2",
				Freetext: "Ale super2",
				Start:    nsShipmentTime,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Starting3",
				Freetext: "Ale super3",
				Start:    nsShipmentTime,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Starting4",
				Freetext: "Ale super4",
				Start:    nsShipmentTime,
			},
		},
	}
}

type MockCampaignProvider3 struct{}

func (cp *MockCampaignProvider3) GetAllCampaigns() []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider3) GetUpcomingCampaigns(t time.Time) []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider3) GetCurrentCampaigns(t time.Time) []campaign.Campaign {
	return []campaign.Campaign{
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current1",
				Freetext: "Ale super1",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current2",
				Freetext: "Ale super2",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current3",
				Freetext: "Ale super3",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current4",
				Freetext: "Ale super4",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current5",
				Freetext: "Ale super5",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current6",
				Freetext: "Ale super6",
			},
		},
	}
}
func (cp *MockCampaignProvider3) GetStartingCampaigns(t time.Time) []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider2) GetUpcomingCampaigns(t time.Time) []campaign.Campaign {
	return nil
}

type MockCampaignProvider4 struct{}

func (cp *MockCampaignProvider4) GetStartingCampaigns(t time.Time) []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider4) GetAllCampaigns() []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider4) GetUpcomingCampaigns(t time.Time) []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider4) GetCurrentCampaigns(t time.Time) []campaign.Campaign {
	return []campaign.Campaign{
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current1",
				Freetext: "Ale super1",
				Start:    nsShipment24HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current2",
				Freetext: "Ale super2",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current3",
				Freetext: "Ale super3",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current4",
				Freetext: "Ale super4",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current5",
				Freetext: "Ale super5",
				Start:    nsShipment7HoursBefore,
			},
		},
	}
}

type MockCampaignProvider5 struct{}

func (cp *MockCampaignProvider5) GetStartingCampaigns(t time.Time) []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider5) GetAllCampaigns() []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider5) GetUpcomingCampaigns(t time.Time) []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider5) GetCurrentCampaigns(t time.Time) []campaign.Campaign {
	return []campaign.Campaign{
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current1",
				Freetext: "Ale super1",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current2",
				Freetext: "Ale super2",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current1",
				Freetext: "Ale super1",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current4",
				Freetext: "Ale super4",
				Start:    nsShipment7HoursBefore,
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Current5",
				Freetext: "Ale super5",
				Start:    nsShipment7HoursBefore,
			},
		},
	}
}

type DefaultTopicTestCase struct {
	Title         string
	Newsletter    Newsletter
	ExpectedTopic string
}

func getNewsletter1() Newsletter {
	nt := Newsletter{
		ShipmentDate:    nsShipmentTime,
		PreheaderOption: Current,
	}
	nt.ProvideCampaigns(&MockCampaignProvider1{})
	return nt
}
func getNewsletter2() Newsletter {
	nt := Newsletter{
		ShipmentDate:    nsShipmentTime,
		PreheaderOption: Starting,
	}
	nt.ProvideCampaigns(&MockCampaignProvider2{})
	return nt
}
func getNewsletter3() Newsletter {
	nt := Newsletter{
		ShipmentDate: nsShipmentTime,
	}
	nt.ProvideCampaigns(&MockCampaignProvider3{})
	return nt
}
func getNewsletter4() Newsletter {
	nt := Newsletter{
		ShipmentDate: nsShipmentTime,
	}
	nt.ProvideCampaigns(&MockCampaignProvider4{})
	return nt
}
func getNewsletter5() Newsletter {
	nt := Newsletter{
		ShipmentDate: nsShipmentTime,
	}
	nt.ProvideCampaigns(&MockCampaignProvider5{})
	return nt
}

func TestNewsletter_GetDefaultTopic(t *testing.T) {

	defaulttopicTestCases := []DefaultTopicTestCase{
		{
			Title:         "Should return text from first 3 starting campaigns, to not exceed 120 character limit",
			Newsletter:    getNewsletter1(),
			ExpectedTopic: "Starting1 - Super freetext kampanii1, Starting2 - Super freetext kampanii2, Starting3 - Super freetext kampanii3",
		},
		{
			Title:         "Should return all starting and one from current to fill 120 characters",
			Newsletter:    getNewsletter2(),
			ExpectedTopic: "Starting1 - Ale super1, Starting2 - Ale super2, Starting3 - Ale super3, Starting4 - Ale super4, Current1 - costam",
		},
		{
			Title:         "should return current campaigns when there are no starting campaigns",
			Newsletter:    getNewsletter3(),
			ExpectedTopic: "Current1 - Ale super1, Current2 - Ale super2, Current3 - Ale super3, Current4 - Ale super4, Current5 - Ale super5",
		},
		{
			Title:         "should firstly return current campaigns that starts at the same date as shipment date, then older current campaigns",
			Newsletter:    getNewsletter4(),
			ExpectedTopic: "Current2 - Ale super2, Current3 - Ale super3, Current4 - Ale super4, Current5 - Ale super5, Current1 - Ale super1",
		},
		{
			Title:         "should not return same campaign name twice if it occurs somewhere more than once",
			Newsletter:    getNewsletter5(),
			ExpectedTopic: "Current1 - Ale super1, Current2 - Ale super2, Current4 - Ale super4, Current5 - Ale super5",
		},
	}

	for _, testCase := range defaulttopicTestCases {
		t.Run(testCase.Title, func(t *testing.T) {
			if testCase.Newsletter.GetDefaultTopic() != testCase.ExpectedTopic {
				t.Errorf("Generated default topic is not as expected!\n what's wrong? => %s \n expected => %s (%d) \n actual => %s (%d)", testCase.Title, testCase.ExpectedTopic, len(testCase.ExpectedTopic), testCase.Newsletter.GetDefaultTopic(), len(testCase.Newsletter.GetDefaultTopic()))
				for _, val := range testCase.Newsletter.Campaigns.Current {
					t.Logf("\n\n%v", val)
				}
			}
		})
	}
}

type DefaultPreheaderTestCase struct {
	Title             string
	Newsletter        Newsletter
	ExpectedPreheader string
}

func TestNewsletter_GetDefaultPreheader(t *testing.T) {
	testCases := []DefaultPreheaderTestCase{
		{
			Title:             "should return campaign names from current",
			Newsletter:        getNewsletter1(),
			ExpectedPreheader: "Current1 * Current2 * Current3 * Current4",
		},
		{
			Title:             "should return campaign names from starting",
			Newsletter:        getNewsletter2(),
			ExpectedPreheader: "Starting1 * Starting2 * Starting3 * Starting4",
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.Title, func(t *testing.T) {
			if testCase.Newsletter.GetDefaultPreheader() != testCase.ExpectedPreheader {
				t.Errorf("Generated default preheader is not as expected!\n what's wrong? => %s \n expected => %s (%d) \n actual => %s (%d)", testCase.Title, testCase.ExpectedPreheader, len(testCase.ExpectedPreheader), testCase.Newsletter.GetDefaultPreheader(), len(testCase.Newsletter.GetDefaultPreheader()))
			}
		})
	}
}
