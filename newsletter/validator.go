package newsletter

import (
	"fmt"

	"bitbucket.org/limango/newsletter_api/logger"

	"gopkg.in/go-playground/validator.v9"
)

// Validate checks if newsletter struct has all required and valid fields
// if something is wrong, Validate returns error
func (ns *Newsletter) Validate() error {

	validate := validator.New()
	if err := validate.Struct(ns); err != nil {
		logger.Log(fmt.Sprintf("validation error: %s \n", err), logger.Error)
		return err.(validator.ValidationErrors)
	}
	return nil
}
