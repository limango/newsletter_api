package newsletter

import (
	"encoding/xml"
	"sort"
	"strconv"
	"time"

	"bitbucket.org/limango/newsletter_api/variable_parser"

	"bitbucket.org/limango/newsletter_api/campaign"
)

var campaignDateLayout = "02.01"

const placeholderInvitingUrl = "https://cdn.limango.pl/images/sare_newsletter/placeholder_inviting_nlt_v01.jpg"
const placeholderEndingUrl = "https://cdn.limango.pl/images/sare_newsletter/placeholder_ending_nlt_v012.jpg"

// zamienic string na datetime type przy odpowiednich polach
type Newsletter struct {
	GroupId               int             `json:"GroupId"`
	VariantId             int             `json:"Id"`
	MailingGroupId        int             `json:"MailingGroupId"`
	Type                  int             `json:"Type"`
	Description           string          `json:"Description"`
	Topic                 string          `json:"Topic"`
	PixelPath             string          `json:"PixelPath"`
	Preheader             string          `json:"Preheader"`
	PreheaderOption       PreheaderOption `json:"PreheaderOption" validate:"required"`
	ShipmentDate          time.Time       `json:"ShipmentDate" validate:"required,gtfield=CreatedAt"`
	CurrentCampaignsCount int             `json:"CurrentCampaignsCount"`
	CampaignsOrderType    int             `json:"CampaignsOrderType"`
	CategoryName          string          `json:"CategoryName"`
	IncludeCampaigns      string          `json:"IncludeCampaigns"`
	ExcludeCampaigns      string          `json:"ExcludeCampaigns"`
	BigImagePath          string          `json:"BigImagePath"`
	BigImageUrl           string          `json:"BigImageUrl"`
	BigImageText          string          `json:"BigImageText"`
	PlaceholderImagePath  string          `json:"PlaceholderImagePath"`
	PlaceholderImageUrl   string          `json:"PlaceholderImageUrl"`
	XmlUrl                string          `json:"XmlUrl"`
	HtmlUrl               string          `json:"HtmlUrl"`
	Status                int             `json:"Status"`
	PromocodeAttachedTo   string          `json:"PromocodeAttachedTo"`
	PromocodeId           int             `json:"PromocodeId"`
	CreatedAt             time.Time       `json:"CreatedAt"`
	DeletedAt             time.Time       `json:"DeletedAt"`
	Ga                    string          `json:"ga"`
	Campaigns             CampaignsNode
	campaignProvider      *campaign.CampaignProvider
	variableParser        variable_parser.VariableParser
}

type CampaignsNode struct {
	Current  CampaignsGroup `xml:"current>campaign"`
	Starting CampaignsGroup `xml:"now>campaign"`
	Upcoming DateGrouping   `xml:"upcomming>date_group"`
}

type DateGrouping []DateGroup

type CampaignsGroup []campaign.Campaign

func (cg *CampaignsGroup) filterByDate(t time.Time) CampaignsGroup {
	return campaign.FilterCampaigns([]campaign.Campaign(*cg), func(c campaign.Campaign) bool {
		return dateEqual(c.Start, t)
	})
}

func dateEqual(t1, t2 time.Time) bool {
	y1, m1, d1 := t1.Date()
	y2, m2, d2 := t2.Date()
	return y1 == y2 && m1 == m2 && d1 == d2
}

// GetCampaignsForDate returns campaigns from node under specified date string, e.g. "01.02"
func (dg DateGrouping) GetCampaignsForDate(date string) []campaign.Campaign {
	for _, val := range dg {
		if val.Date == date {
			return val.Campaigns
		}
	}
	return nil
}

// GetCampaigns return all campaigns from node, regardless of date
func (dg DateGrouping) GetCampaigns() []campaign.Campaign {
	result := []campaign.Campaign{}
	for _, campaignGroup := range dg {
		for _, camp := range campaignGroup.Campaigns {
			result = append(result, camp)
		}
	}
	return result
}

type DateGroup struct {
	XMLName   xml.Name            `xml:"date_group"`
	Date      string              `xml:"date,attr"`
	DateName  string              `xml:"date_name,attr"`
	Campaigns []campaign.Campaign `xml:"campaign"`
	Time      time.Time           `xml:"-"`
}

type PreheaderOption int

const (
	Current  PreheaderOption = 1
	Starting                 = 2
)

func (nt *Newsletter) ProvideCampaigns(provider campaign.CampaignProvider) {
	nt.Campaigns.Current = provider.GetCurrentCampaigns(nt.ShipmentDate)
	nt.Campaigns.Starting = provider.GetStartingCampaigns(nt.ShipmentDate)
	nt.Campaigns.Upcoming = mapCampaignsByDates(provider.GetUpcomingCampaigns(nt.ShipmentDate))

	nt.sortCampaigns()
	nt.adjustCampaignsData()

	nt.campaignProvider = &provider
}

func (nt *Newsletter) sortCampaigns() {
	sort.Sort(ByDate(nt.Campaigns.Upcoming))
	for _, dateGroup := range nt.Campaigns.Upcoming {
		sort.Sort(campaign.ByPosition(dateGroup.Campaigns))
	}
	sort.Sort(campaign.ByPosition(nt.Campaigns.Current))
	sort.Sort(campaign.ByPosition(nt.Campaigns.Starting))
}

// adjustCampaignsData performs additional logic needed for proper xml structure
func (nt *Newsletter) adjustCampaignsData() {
	for key, dateGroup := range nt.Campaigns.Upcoming {
		maxUpcomingLength := 4
		if len(dateGroup.Campaigns) < maxUpcomingLength {
			maxUpcomingLength = len(dateGroup.Campaigns)
		}
		nt.Campaigns.Upcoming[key].Campaigns = dateGroup.Campaigns[:maxUpcomingLength]
	}

	// add placeholders when odd number of campaigns in node
	if len(nt.Campaigns.Current)%2 != 0 {
		nt.Campaigns.Current = append(nt.Campaigns.Current, campaign.Campaign{
			Url:            placeholderEndingUrl,
			CustomImageUrl: "https://www.limango.pl/g.php?H=%accesscode%&ref=/home?order=ending&utm_source=limango&utm_medium=newsletter&utm_campaign=" + nt.ShipmentDate.Format("2006-01-02") + "&utm_content=ending_current&utm_term=%mailing%",
		})
	}

	if len(nt.Campaigns.Starting)%2 != 0 {
		if nt.PlaceholderImagePath != "" && nt.PlaceholderImageUrl != "" {
			nt.Campaigns.Current = append(nt.Campaigns.Current, campaign.Campaign{
				Url:            nt.PlaceholderImageUrl,
				CustomImageUrl: nt.PlaceholderImagePath,
			})
		} else {
			nt.Campaigns.Current = append(nt.Campaigns.Current, campaign.Campaign{
				Url:            placeholderInvitingUrl,
				CustomImageUrl: "https://www.limango.pl/g.php?H=%accesscode%&ref=/konto/zaproszenia&utm_source=limango&utm_medium=newsletter&utm_campaign=" + nt.ShipmentDate.Format("2006-01-02") + "&utm_content=inviting_new&utm_term=%mailing%",
			})
		}
	}
}

func (nt *Newsletter) GetPixelPath() string {
	if nt.PixelPath == "" {
		nt.PixelPath = "https://cdn.limango.pl/images/sare_newsletter/pixel_" + strconv.Itoa(nt.GroupId) + "_" + nt.ShipmentDate.Format("20060102") + ".jpg"
	}
	return nt.PixelPath
}

func (nt *Newsletter) GetTopic() string {
	if nt.Topic == "" {
		nt.Topic = nt.GetDefaultTopic()
	}
	return nt.Topic
}

func (nt *Newsletter) GetPreheader() string {
	if nt.Preheader == "" {
		nt.Preheader = nt.GetDefaultPreheader()
	}
	return nt.Preheader
}

func (nt *Newsletter) GetGA() GA {
	return GA{
		UtmSource:   CData{"limango"},
		UtmMedium:   CData{"newsletter"},
		UtmCampaign: CData{nt.ShipmentDate.Format("20060102")},
		UtmTerm:     CData{"%mailing%"},
	}
}

func (nt *Newsletter) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	nt.provideDataForCampaigns()
	return e.Encode(nt.createXmlRepresentation())
}

// mapCampaignsByDates groups slice of campaigns by their start date into CampaignsGroup
func mapCampaignsByDates(campaigns []campaign.Campaign) DateGrouping {
	resultMap := make(map[string][]campaign.Campaign)
	for _, c := range campaigns {
		date := c.Start.Format(campaignDateLayout)
		resultMap[date] = append(resultMap[date], c)
	}

	result := DateGrouping{}
	for k, v := range resultMap {
		result = append(result, DateGroup{
			Date:      k,
			DateName:  getDateName(v[0].Start),
			Campaigns: v,
			Time:      v[0].Start,
		})
	}
	return result
}

func getDateName(t time.Time) string {
	return getPLWeekDay(t.Weekday())
}

func getPLWeekDay(day time.Weekday) string {
	switch day {
	case time.Monday:
		return "Poniedziałek"
	case time.Tuesday:
		return "Wtorek"
	case time.Wednesday:
		return "Środa"
	case time.Thursday:
		return "Czwartek"
	case time.Friday:
		return "Piątek"
	case time.Saturday:
		return "Sobota"
	case time.Sunday:
		return "Niedziela"
	default:
		return "Unknown"
	}
}

// This function is called to make sure that campaigns have all necessary data when it's time to generate newsletter XML
func (nt *Newsletter) provideDataForCampaigns() {
	for _, camp := range nt.Campaigns.Current {
		camp.NewsletterNode = "current"
		camp.NewsletterGroupID = nt.GroupId
		camp.NewsletterShippingDate = nt.ShipmentDate
	}
	for _, camp := range nt.Campaigns.Starting {
		camp.NewsletterNode = "now"
		camp.NewsletterGroupID = nt.GroupId
		camp.NewsletterShippingDate = nt.ShipmentDate
	}
	for _, dateGroup := range nt.Campaigns.Upcoming {
		for _, camp := range dateGroup.Campaigns {
			camp.NewsletterNode = "upcomming"
			camp.NewsletterGroupID = nt.GroupId
			camp.NewsletterShippingDate = nt.ShipmentDate
		}
	}
}

type NewsletterXmlRepresentaton struct {
	XMLName            xml.Name      `xml:"newsletter_data"`
	Topic              CData         `xml:"topic"`
	PixelPath          CData         `xml:"pixel_path"`
	Preheader          CData         `xml:"preheader"`
	CreatedAt          CData         `xml:"created_at"`
	ShipmentDate       CData         `xml:"shipment_date"`
	CategoryName       CData         `xml:"category_name"`
	IncludeCampaigns   CData         `xml:"include_campaigns"`
	ExcludeCampaigns   CData         `xml:"exclude_campaigns"`
	BigImgPath         CData         `xml:"big_img_path"`
	BigImgUrl          CData         `xml:"big_img_url"`
	BigImgText         CData         `xml:"big_img_text"`
	PlaceholderImgPath CData         `xml:"placeholder_img_path"`
	PlaceholderImgUrl  CData         `xml:"placeholder_img_url"`
	Ga                 GA            `xml:"ga"`
	Campaigns          CampaignsNode `xml:"campaigns"`
}

type CData struct {
	Value string `xml:",cdata"`
}

type GA struct {
	UtmSource   CData `xml:"utm_source"`
	UtmMedium   CData `xml:"utm_medium"`
	UtmCampaign CData `xml:"utm_campaign"`
	UtmTerm     CData `xml:"utm_term"`
}

func (nt *Newsletter) createXmlRepresentation() NewsletterXmlRepresentaton {
	return NewsletterXmlRepresentaton{
		Topic:              CData{nt.getVariableParser().Parse(nt.GetTopic())},
		PixelPath:          CData{nt.GetPixelPath()},
		Preheader:          CData{nt.getVariableParser().Parse(nt.GetPreheader())},
		CreatedAt:          CData{strconv.FormatInt(nt.CreatedAt.Unix(), 10)},
		ShipmentDate:       CData{strconv.FormatInt(nt.ShipmentDate.Unix(), 10)},
		CategoryName:       CData{nt.CategoryName},
		IncludeCampaigns:   CData{nt.IncludeCampaigns},
		ExcludeCampaigns:   CData{nt.ExcludeCampaigns},
		BigImgPath:         CData{nt.BigImagePath},
		BigImgUrl:          CData{nt.BigImageUrl},
		BigImgText:         CData{nt.getVariableParser().Parse(nt.BigImageText)},
		PlaceholderImgPath: CData{nt.PlaceholderImagePath},
		PlaceholderImgUrl:  CData{nt.PlaceholderImageUrl},
		Ga:                 nt.GetGA(),
		Campaigns:          nt.Campaigns,
	}
}

func (nt *Newsletter) getVariableParser() variable_parser.VariableParser {
	if nt.variableParser == nil {
		nt.variableParser = variable_parser.NewVariableParser(nt.campaignProvider, nt.ShipmentDate, nt.PromocodeId)
	}
	return nt.variableParser
}

type ByDate []DateGroup

func (groups ByDate) Len() int {
	return len(groups)
}

func (groups ByDate) Swap(i, j int) {
	groups[i], groups[j] = groups[j], groups[i]
}

func (groups ByDate) Less(i, j int) bool {
	return groups[i].Time.Before(groups[j].Time)
}
