package newsletter

import (
	"strings"

	"bitbucket.org/limango/newsletter_api/campaign"
)

// GetDefaultTopic generates topic string from campaign names and their free texts
func (nt *Newsletter) GetDefaultTopic() string {
	result := ""

	// first get all campaigns that are starting at the moment the newsletter is being sent
	for _, camp := range nt.Campaigns.Starting {
		if campaignWillFitToTopic(camp, result) {
			result = addCampaignToTopic(camp, result)
		}
	}
	// then, if there's still space, get campaigns started today
	for _, camp := range nt.Campaigns.Current.filterByDate(nt.ShipmentDate) {
		if campaignWillFitToTopic(camp, result) {
			result = addCampaignToTopic(camp, result)
		}
	}
	// finally, if there's still space to fill, add campaigns from yesterday
	shipmentDateMinusOneDate := nt.ShipmentDate.AddDate(0, 0, -1)
	for _, camp := range nt.Campaigns.Current.filterByDate(shipmentDateMinusOneDate) {
		if campaignWillFitToTopic(camp, result) {
			result = addCampaignToTopic(camp, result)
		}
	}

	return result
}

func (nt *Newsletter) GetDefaultPreheader() string {
	preheader := ""
	for _, camp := range nt.GetCampaignsForPreheader() {
		campName := strings.Trim(camp.Name, " ")
		if campaignNameWillFitToPreheader(campName, preheader) {
			preheader = addCampaignToPreheader(campName, preheader)
		}
	}
	return preheader
}

// Topic must be < 120 characters and single campaign name should not occur more than once
func campaignWillFitToTopic(camp campaign.Campaign, topic string) bool {
	campaignTopic := getTopicStrFromCampaign(camp)
	return !strings.Contains(topic, campaignTopic) && len(topic)+2+len(campaignTopic) < 120 // 2 for ", " separator
}

func addCampaignToTopic(campaign campaign.Campaign, topic string) string {
	if len(topic) > 0 {
		return topic + ", " + getTopicStrFromCampaign(campaign)
	}
	return getTopicStrFromCampaign(campaign)
}

func getTopicStrFromCampaign(camp campaign.Campaign) string {
	if len(camp.Freetext) == 0 {
		return ""
	}
	return strings.Trim(camp.Name+" - "+camp.Freetext, " ")
}

func (nt *Newsletter) GetCampaignsForPreheader() []campaign.Campaign {
	switch nt.PreheaderOption {
	case Current:
		return nt.Campaigns.Current
	case Starting:
		return nt.Campaigns.Starting
	}
	return nil
}

func campaignNameWillFitToPreheader(campName string, preheader string) bool {
	return len(campName)+len(preheader)+3 < 200 && !strings.Contains(preheader, campName) // 3 is size of string " * "
}

func addCampaignToPreheader(campName string, preheader string) string {
	if len(preheader) > 0 {
		return preheader + " * " + campName
	}
	return campName
}
