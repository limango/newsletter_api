package service

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/joho/godotenv"

	"bitbucket.org/limango/newsletter_api/campaign"

	"bitbucket.org/limango/newsletter_api/newsletter"
	"bitbucket.org/limango/newsletter_api/storage"
)

const emptyId = ""
const correctId = "123"
const secondId = "321"

const notExistingKey = "not_existing_key"
const existingKey = "existing_key"
const failingKey = "failing_key"

const existingValue = "existing_value"

type MockStorage struct{}

func (m *MockStorage) Set(key string, value []byte) error {
	return nil
}

func (m *MockStorage) Get(key string) ([]byte, error) {
	if key == notExistingKey {
		return nil, storage.ItemNotFoundError
	}
	if key == failingKey {
		return nil, storage.FailedToFetchData
	}
	res := "existing_value"
	return []byte(res), nil
}

var s *Service

func setUp() {
	if err := godotenv.Load("../.env"); err != nil { // need to specify relative path when on test run
		fmt.Printf("Error when loading dotenv files: %s\n", err.Error())
		os.Exit(1)
	}

	stor := MockStorage{}
	campaignProvider := &MockCampaignProvider1{}
	s = NewService(ServiceOptions{
		Storage:          &stor,
		Version:          "vTest",
		CampaignProvider: campaignProvider,
	})
}

type MockCampaignProvider1 struct{}

func (cp *MockCampaignProvider1) GetAllCampaigns() []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider1) GetUpcomingCampaigns(t time.Time) []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider1) GetCurrentCampaigns(t time.Time) []campaign.Campaign {
	return nil
}
func (cp *MockCampaignProvider1) GetStartingCampaigns(t time.Time) []campaign.Campaign {
	return []campaign.Campaign{
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Starting1",
				Freetext: "Super freetext kampanii1",
				Start:    time.Now(),
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Starting2",
				Freetext: "Super freetext kampanii2",
				Start:    time.Now(),
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Starting3",
				Freetext: "Super freetext kampanii3",
				Start:    time.Now(),
			},
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name:     "Starting4",
				Freetext: "Super freetext kampanii4",
				Start:    time.Now(),
			},
		},
	}
}

func TestGetNewsletterWithEmptyIdShouldReturnBadIdError(t *testing.T) {

	setUp()

	if _, err := s.GetNewsletter(emptyId); err != BadIdError {
		t.Errorf("should return BadId error: %v", err)
	}
}

func TestGetNewsletterWithCorrectIdShouldNotReturnError(t *testing.T) {
	setUp()
	if _, err := s.GetNewsletter(correctId); err != nil {
		t.Errorf("should not return error: %v", err)
	}
}

func TestTwoIdsShouldGenerateDifferentKeys(t *testing.T) {
	setUp()

	key1 := s.generateKey(correctId)
	key2 := s.generateKey(secondId)

	if key1 == key2 {
		t.Errorf("keys generated should be different: %v, %v", key1, key2)
	}
}

func TestGetExistingNewsletter(t *testing.T) {
	setUp()
	if res, err := s.GetNewsletter(existingKey); err != nil || res != existingValue {
		t.Errorf("should return existing value")
	}
}

func Test(t *testing.T) {
	setUp()
	if res, err := s.GetNewsletter(existingKey); err != nil || res != existingValue {
		t.Errorf("should return existing value")
	}
}

type TestJsonNewsletter struct {
	title     string
	jsonInput []byte
	output    error
}

func TestSaveNewsletterJsons(t *testing.T) {
	setUp()

	validJson, _ := json.Marshal(newsletter.Newsletter{
		VariantId:       50,
		Topic:           "asd",
		PreheaderOption: 2,
		ShipmentDate:    time.Now(),
	})

	testInputs := []TestJsonNewsletter{
		{
			title:     "should return error when input is empty",
			jsonInput: []byte(""),
			output:    EmptyInputError,
		},
		{
			title:     "invalid json should return error",
			jsonInput: []byte("{"),
			output:    InvalidJsonString,
		},
		{
			title:     "invalid json should return error",
			jsonInput: []byte("\"Michal\": 1"),
			output:    InvalidJsonString,
		},
		{
			title:     "empty json should return error",
			jsonInput: []byte("{}"),
			output:    NewsletterValidationError,
		},
		{
			title:     "incorrect json structure should return error",
			jsonInput: []byte("{\"asd\": 1}"),
			output:    NewsletterValidationError,
		},
		{
			title:     "valid json should not return error",
			jsonInput: validJson,
			output:    nil,
		},
	}

	for _, test := range testInputs {
		t.Run(test.title, func(t *testing.T) {

			if _, err := s.SaveNewsletter(test.jsonInput); err != test.output {
				t.Errorf("\n%v \nerror => %v \njsonInput => %s ", test.title, err, test.jsonInput)
			}
		})
	}
}

var newsletterJson = `
{  
   "Id":2974,
   "GroupId":327,
   "MailingGroupId":null,
   "Type":1,
   "Description":"test",
   "Topic":"test",
   "Preheader":"preheader test",
   "PreheaderOption":2,
   "ShipmentDate":"2018-11-29T07:00:00+01:00",
   "CurrentCampaignsCount":null,
   "CampaignsOrderType":1,
   "BigImageUrl":null,
   "BigImageText":"test",
   "BigImagePath":null,
   "PlaceholderImageUrl":null,
   "PlaceholderImagePath":null,
   "XmlUrl":"xml\/feed\/327_20181128.xml",
   "HtmlUrl":"html\/newsletter\/327.html",
   "Status":1,
   "PromocodeAttachedTo":"0",
   "PromocodeId":null,
   "CreatedAt":"2018-11-29T06:02:55+01:00",
   "DeletedAt":null,
   "BigImages":[  
      {  
         "Id":null,
         "VariantId":2974,
         "FilePath":null,
         "FileUri":null,
         "PromocodeId":null,
         "Position":0,
         "CreatedAt":null,
         "DeletedAt":null,
         "Variant":"*RECURSION*"
      }
   ],
   "VariantCampaigns":[  

   ]
}
`

var secondNsJson = `
{  
   "Id":8169,
   "GroupId":415,
   "MailingGroupId":null,
   "Type":1,
   "Description":"test ania - dzielony bigimg",
   "Topic":"jaki\u015b super temat",
   "Preheader":null,
   "PreheaderOption":1,
   "ShipmentDate":"2018-12-20T07:00:00+01:00",
   "CurrentCampaignsCount":null,
   "CampaignsOrderType":1,
   "BigImageUrl":"https:\/\/www.google.pl\/",
   "BigImageText":"<!DOCTYPE html PUBLIC \"-\/\/W3C\/\/DTD HTML 4.0 Transitional\/\/EN\" \"http:\/\/www.w3.org\/TR\/REC-html40\/loose.dtd\">\n<html><head><meta http-equiv=\"Content-Type\" content=\"charset=utf-8\"><\/head><body><strong>gruby tekst\u00a0<\/strong><em>\u00a0<br>i jeszcze jaki\u015b chudy tekst\u00a0<br><\/em><\/body><\/html>\n",
   "BigImagePath":"http:\/\/cdn.limango.pl\/images\/sare_newsletter\/koszyk.png",
   "PlaceholderImageUrl":"https:\/\/www.google.pl\/",
   "PlaceholderImagePath":"http:\/\/cdn.limango.pl\/images\/sare_newsletter\/retail.png",
   "XmlUrl":"xml\/feed\/415_20181219.xml",
   "HtmlUrl":null,
   "Status":null,
   "PromocodeAttachedTo":"0",
   "PromocodeId":null,
   "CreatedAt":"2018-12-19T13:26:32+01:00",
   "DeletedAt":null,
   "ParentId":null,
   "FreeDeliveryValidTo":null,
   "FreeDeliveryValidityValue":null,
   "FreeDeliveryValidityUnit":"day",
   "CategoryName":"All",
   "IncludeCampaigns":"34705,40779",
   "ExcludeCampaigns":""
}
`

func TestParsingNewsletterFromJson(t *testing.T) {
	setUp()

	nt, err := s.GetNewsletterFromJson([]byte(newsletterJson))

	if err != nil {
		t.Errorf("Service could not parse valid newsletter json")
	}

	if nt.Topic != "test" {
		t.Errorf("Topic of the newsletter was not correctly parsed from json")
	}

	if nt.CreatedAt.Format("2006-01-02 15:04:05") != "2018-11-29 06:02:55" {
		t.Errorf("Service could not parse valid newsletter json")
	}

	nt, err = s.GetNewsletterFromJson([]byte(secondNsJson))
	if err != nil {
		t.Errorf("Service could not parse valid newsletter json: \n err => %s", err)
	}
}

// dopisac testy walidujace newsletter z jsona
