package service

import "fmt"

var BadIdError = fmt.Errorf("Id should be non empty value")

var EmptyInputError = fmt.Errorf("input should not be empty")
var InvalidJsonString = fmt.Errorf("json structure is not correct json")
var NewsletterValidationError = fmt.Errorf("validation error: some of the fields parsed from json don't fulfill validation constraints")
