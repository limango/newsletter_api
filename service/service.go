package service

import (
	"encoding/json"
	"encoding/xml"
	"strconv"
	"strings"
	"time"

	"bitbucket.org/limango/newsletter_api/campaign"
	"bitbucket.org/limango/newsletter_api/newsletter"
	"bitbucket.org/limango/newsletter_api/storage"
	"gopkg.in/go-playground/validator.v9"
)

type NewsletterService interface {
	GetNewsletter(id string) (string, error)
	SaveNewsletter(newsletterJson []byte) (*newsletter.Newsletter, error)
	GetNewsletterFromJson(newsletterJson []byte) (*newsletter.Newsletter, error)
}

type Service struct {
	storage          storage.Storage
	version          string
	campaignProvider campaign.CampaignProvider
}

type ServiceOptions struct {
	storage.Storage
	Version          string
	CampaignProvider campaign.CampaignProvider
}

func NewService(options ServiceOptions) *Service {
	return &Service{
		storage:          options.Storage,
		version:          options.Version,
		campaignProvider: options.CampaignProvider,
	}
}

func (s Service) GetNewsletter(id string) (string, error) {

	if len(id) == 0 {
		return "", BadIdError
	}

	key := s.generateKey(id)
	if val, err := s.storage.Get(key); err == nil && val != nil {
		return string(val), nil
	}

	return "", nil
}

func (s Service) SaveNewsletter(newsletterJson []byte) (*newsletter.Newsletter, error) {
	if len(newsletterJson) == 0 {
		return nil, EmptyInputError
	}
	if !s.isValidJson(string(newsletterJson)) {
		return nil, InvalidJsonString
	}

	nt := newsletter.Newsletter{}
	if err := json.Unmarshal(newsletterJson, &nt); err != nil {
		return nil, err
	}
	if err := nt.Validate(); err != nil {
		return nil, NewsletterValidationError
	}
	nt.ProvideCampaigns(s.campaignProvider)

	newsletterXml, err := xml.Marshal(&nt)
	if err != nil {
		return nil, err
	}

	// if newsletter with today's date -> upload as default
	if nt.ShipmentDate.Format("2006-01-02") == time.Now().Format("2006-01-02") {
		key := s.generateKey(strconv.Itoa(nt.GroupId))
		s.storage.Set(key, newsletterXml)
	}

	// upload as variant for given shipment date
	key := s.generateKey(strconv.Itoa(nt.GroupId) + "_" + nt.ShipmentDate.Format("20060102"))
	return &nt, s.storage.Set(key, newsletterXml)
}

func (s *Service) GetNewsletterFromJson(newsletterJson []byte) (*newsletter.Newsletter, error) {
	res := newsletter.Newsletter{}
	if err := json.Unmarshal(newsletterJson, &res); err != nil {
		if validationError, ok := err.(validator.ValidationErrors); ok {
			return nil, validationError
		}
		return nil, err
	}
	return &res, nil
}

func (s Service) generateKey(id string) string {
	return strings.Join(
		[]string{
			"xml/feed/",
			id,
			".xml",
		},
		"",
	)
}

func (s *Service) isValidJson(input string) bool {
	var js map[string]interface{}
	return json.Unmarshal([]byte(input), &js) == nil
}
