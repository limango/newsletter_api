package variable_parser

import (
	"fmt"
	"log"
	"strconv"
	"time"

	"bitbucket.org/limango/newsletter_api/logger"

	"bitbucket.org/limango/tools"

	"github.com/jmoiron/sqlx"

	"bitbucket.org/limango/newsletter_api/campaign"
)

type VariableFetcher interface {
	GetCampaignName(pos int, node string) string
	GetCampaignDiscount(pos int, node string) string
	GetCampaignDescription(pos int, node string) string
	GetVoucherValue() string
	GetMinimalOrderValueForVoucher() string
	GetVoucherCode() string
	GetVoucherExpirationDate() string
	GetVoucherExpirationDateNaturalLang() string
	GetVoucherExpirationDateNaturalLangAccusative() string
	GetVoucherCountLimit() string
	GetVoucherExpirationDateFormatB() string
	GetVoucherLifetime() string
}

func NewVariablesFetcher(provider campaign.CampaignProvider, shipmentTime time.Time, voucherID int) VariableFetcher {
	return VariableFetcherImpl{
		provider:     &provider,
		shipmentTime: shipmentTime,
		voucherID:    voucherID,
	}
}

type VariableFetcherImpl struct {
	provider     *campaign.CampaignProvider
	shipmentTime time.Time
	voucherID    int
	voucher      *Voucher
}

type Voucher struct {
	Id             int
	Value          string
	Code           string
	MinimalOrder   string    `db:"min_order"`
	ExpirationDate time.Time `db:"validto"`
	MaxUsage       int       `db:"max_usage"`
	ValidityUnit   string    `db:"voucher_validity_unit"`
	ValidityValue  int       `db:"voucher_validity_value"`
}

func (vf VariableFetcherImpl) GetCampaignName(pos int, node string) string {
	return vf.GetCampaignAtPosition(pos, node).Name
}

func (vf VariableFetcherImpl) GetCampaignDiscount(pos int, node string) string {
	if vf.GetCampaignAtPosition(pos, node).Discount == 0 {
		return ""
	}
	return fmt.Sprintf("do -%d%%", vf.GetCampaignAtPosition(pos, node).Discount)
}

func (vf VariableFetcherImpl) GetCampaignDescription(pos int, node string) string {
	return vf.GetCampaignAtPosition(pos, node).Description
}

func (vf VariableFetcherImpl) GetVoucherValue() string {
	return vf.GetVoucher().Value
}

func (vf VariableFetcherImpl) GetMinimalOrderValueForVoucher() string {
	return vf.GetVoucher().MinimalOrder
}

func (vf VariableFetcherImpl) GetVoucherCode() string {
	return vf.GetVoucher().Code
}

func (vf VariableFetcherImpl) GetVoucherExpirationDate() string {
	if vf.GetVoucher().ExpirationDate.IsZero() {
		return ""
	}
	return vf.GetVoucher().ExpirationDate.Format("2006-01-02 15:04:05")
}

func (vf VariableFetcherImpl) GetVoucherExpirationDateNaturalLang() string {
	if vf.GetVoucher().ExpirationDate.IsZero() {
		return ""
	}
	if vf.GetVoucher().ExpirationDate.Format("2006-01-02") == time.Now().Format("2006-01-02") {
		return "dzisiaj"
	}
	if vf.GetVoucher().ExpirationDate.Format("2006-01-02") == time.Now().AddDate(0, 0, 1).Format("2006-01-02") {
		return "jutro"
	}
	return vf.GetVoucher().ExpirationDate.Format("2006-01-02 15:04:05")
}

func (vf VariableFetcherImpl) GetVoucherExpirationDateNaturalLangAccusative() string {
	if vf.GetVoucher().ExpirationDate.IsZero() {
		return ""
	}
	if vf.GetVoucher().ExpirationDate.Format("2006-01-02") == time.Now().Format("2006-01-02") {
		return "dzisiaj"
	}
	if vf.GetVoucher().ExpirationDate.Format("2006-01-02") == time.Now().AddDate(0, 0, 1).Format("2006-01-02") {
		return "jutra"
	}
	return vf.GetVoucher().ExpirationDate.Format("2006-01-02 15:04:05")
}

func (vf VariableFetcherImpl) GetVoucherExpirationDateFormatB() string {
	if vf.GetVoucher().ExpirationDate.IsZero() {
		return ""
	}
	return vf.GetVoucher().ExpirationDate.Format("02.01.2006")
}

func (vf VariableFetcherImpl) GetVoucherCountLimit() string {
	return strconv.Itoa(vf.GetVoucher().MaxUsage)
}

func (vf VariableFetcherImpl) GetVoucherLifetime() string {
	v := vf.GetVoucher()
	if v.ValidityUnit != "day" || v.ValidityValue == 0 {
		return ""
	}
	return strconv.Itoa(v.ValidityValue)
}

// GetCampaignAtPosition returns campaign for specified position and node of campaigns
// position must be positive integer
// node must be of of strings: "now", "upcoming", "current"
func (vf VariableFetcherImpl) GetCampaignAtPosition(position int, node string) campaign.Campaign {
	provider := *vf.provider
	camps := []campaign.Campaign{}

	switch node {
	case "now":
		camps = provider.GetStartingCampaigns(vf.shipmentTime)
	case "upcoming":
		camps = provider.GetUpcomingCampaigns(vf.shipmentTime)
	case "current":
		camps = provider.GetCurrentCampaigns(vf.shipmentTime)
	}

	if position > 0 && position <= len(camps) {
		return camps[position-1]
	}
	return campaign.Campaign{} // default empty
}

func (vf *VariableFetcherImpl) GetVoucher() Voucher {
	if vf.voucher == nil {
		vf.voucher = fetchVoucherFromDB(vf.voucherID)
	}
	return *vf.voucher
}

func fetchVoucherFromDB(id int) *Voucher {
	voucher := &Voucher{}
	db, err := sqlx.Connect("mysql", tools.Env("MYSQL_CONNECTION_STRING", "admin:d99nYVJa7ew8U9xs@tcp(pl-limango-prod.cicvvamgish2.eu-central-1.rds.amazonaws.com)/baza60_limango?parseTime=true"))
	if err != nil {
		log.Fatalln(err)
	}
	err = db.Get(voucher, "SELECT id, code, value, min_order, validto, max_usage FROM li_voucher_static WHERE ID = ?", strconv.Itoa(id))
	if err != nil {
		logger.Log(fmt.Sprintf("Error when querying DB for voucher: %s", err), logger.Warning)
	}
	return voucher
}
