package variable_parser

import (
	"fmt"
	"testing"
	"time"

	"bitbucket.org/limango/newsletter_api/campaign"
)

type MockCampaignProvider struct{}

func (MockCampaignProvider) GetAllCampaigns() []campaign.Campaign {
	return nil
}
func (MockCampaignProvider) GetCurrentCampaigns(t time.Time) []campaign.Campaign {
	return []campaign.Campaign{
		campaign.Campaign{
			Core: campaign.Core{
				Name: "current1",
			},
			Discount:    1,
			Description: "description c1",
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name: "current2",
			},
			Discount:    2,
			Description: "description c2",
		},
	}
}
func (MockCampaignProvider) GetStartingCampaigns(t time.Time) []campaign.Campaign {
	return []campaign.Campaign{
		campaign.Campaign{
			Core: campaign.Core{
				Name: "starting1",
			},
			Discount:    1,
			Description: "description s1",
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name: "starting2",
			},
			Discount:    2,
			Description: "description s2",
		},
	}
}
func (MockCampaignProvider) GetUpcomingCampaigns(t time.Time) []campaign.Campaign {
	return []campaign.Campaign{
		campaign.Campaign{
			Core: campaign.Core{
				Name: "upcoming1",
			},
			Discount:    1,
			Description: "description u1",
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name: "upcoming2",
			},
			Discount:    2,
			Description: "description u2",
		},
		campaign.Campaign{
			Core: campaign.Core{
				Name: "upcoming2",
			},
			// no discount here
			Description: "description u3",
		},
	}
}

var provider = MockCampaignProvider{}
var variablesFetcher = NewVariablesFetcher(provider, time.Now(), 22)

type VariablesFetcherTestCase struct {
	Function       string
	Node           string
	Position       int
	ExpectedOutput string
}

func TestGetComplexVars(t *testing.T) {
	var testCases = []VariablesFetcherTestCase{
		{
			Function:       "name",
			Node:           "now",
			Position:       1,
			ExpectedOutput: "starting1",
		},
		{
			Function:       "description",
			Node:           "upcoming",
			Position:       1,
			ExpectedOutput: "description u1",
		},
		{
			Function:       "discount",
			Node:           "now",
			Position:       2,
			ExpectedOutput: "do -2%",
		},
		{
			Function:       "name",
			Node:           "now",
			Position:       4, // not existing campaign
			ExpectedOutput: "",
		},
		{
			Function:       "discount",
			Node:           "upcoming",
			Position:       3,
			ExpectedOutput: "", // campaign without discount
		},
	}

	for _, testCase := range testCases {
		t.Run("Test variables fetcher", func(t *testing.T) {
			output := ""
			switch testCase.Function {
			case "name":
				output = variablesFetcher.GetCampaignName(testCase.Position, testCase.Node)
			case "description":
				output = variablesFetcher.GetCampaignDescription(testCase.Position, testCase.Node)
			case "discount":
				output = variablesFetcher.GetCampaignDiscount(testCase.Position, testCase.Node)
			}

			if output != testCase.ExpectedOutput {
				t.Errorf("Wrong output:\n method => %s\n expected output => %s\n actual output => %s", fmt.Sprintf("%s(%d, %s)", testCase.Function, testCase.Position, testCase.Node), testCase.ExpectedOutput, output)
			}
		})
	}
}

func TestGetVoucher(t *testing.T) {
	voucherCode := variablesFetcher.GetVoucherCode()
	expectedCode := "SZALZAKUPOW"
	if voucherCode != expectedCode {
		t.Errorf("failed get voucher code.\n expected code: %s\n got: %s", expectedCode, voucherCode)
	}
}
