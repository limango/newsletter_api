package variable_parser

import (
	"strconv"
	"testing"

	"bitbucket.org/limango/tools"
)

type MockVariableFetcher struct{}

func (MockVariableFetcher) GetCampaignName(pos int, node string) string {
	return "{campaign_name?pos=" + strconv.Itoa(pos) + "&type=" + node + "}"
}
func (MockVariableFetcher) GetCampaignDiscount(pos int, node string) string {
	return "{discount?pos=" + strconv.Itoa(pos) + "&type=" + node + "}"
}
func (MockVariableFetcher) GetCampaignDescription(pos int, node string) string {
	return "{description?pos=" + strconv.Itoa(pos) + "&type=" + node + "}"
}
func (MockVariableFetcher) GetVoucherValue() string {
	return "{voucher_value}"
}
func (MockVariableFetcher) GetMinimalOrderValueForVoucher() string {
	return "{minimal_order_value}"
}
func (MockVariableFetcher) GetVoucherCode() string {
	return "{voucher_code}"
}
func (MockVariableFetcher) GetVoucherExpirationDate() string {
	return "{expiration_date}"
}
func (MockVariableFetcher) GetVoucherExpirationDateNaturalLang() string {
	return "{expiration_date_natural}"
}
func (MockVariableFetcher) GetVoucherExpirationDateNaturalLangAccusative() string {
	return "{expiration_date_natural_accusative}"
}
func (MockVariableFetcher) GetVoucherCountLimit() string {
	return "{voucher_count_limit}"
}
func (MockVariableFetcher) GetVoucherExpirationDateFormatB() string {
	panic("implement me")
}
func (MockVariableFetcher) GetVoucherLifetime() string {
	panic("implement me")
}
func newMockVariableFetcher() VariableFetcher {
	return MockVariableFetcher{}
}

var variableParser = VariableParserImpl{
	variablesFetcher: newMockVariableFetcher(),
}

func TestNoPlaceholder(t *testing.T) {
	input := "To jest tekst bez zadnego placeholder'a"

	if variableParser.ContainsPlaceholder(input) {
		t.Errorf("%s should not contain any placeholder\n", input)
	}
}

func TestContainsPlaceholder(t *testing.T) {
	input := "Tutaj " + voucherValueKey + " jest placeholder"

	if !variableParser.ContainsPlaceholder(input) {
		t.Errorf("%s should contain placeholder\n", input)
	}
}

func TestContainsComplexPlaceholder(t *testing.T) {
	input := "Tutaj %cn?p=1&n=now% jest placeholder"

	if !variableParser.ContainsPlaceholder(input) {
		t.Errorf("%s should contain placeholder\n", input)
	}
}

func TestStringPositions(t *testing.T) {
	input := "To jest jakiś string %c z polskimi znakami i substringami %c"
	positions := variableParser.getAllPositionsOfString(input, "%c")
	correctAnswer := []int{21, 58}

	if !tools.IntSlicesEqual(positions, correctAnswer) {
		t.Errorf("Variable parser fails to find all string positions: \n correct: %v\nreturned: %v\n", correctAnswer, positions)
	}

	input = "tutaj %cn?p=1&n=now% i tu %cn?p=2&n=now%"
	expected := []int{6, 26}
	actual := variableParser.getAllPositionsOfString(input, "%c")

	if !tools.IntSlicesEqual(expected, actual) {
		t.Errorf("Variable parser fails to find all string positions: \n expected: %v\nactual: %v\n", expected, actual)
	}
}

type ExtractMethodFromVariableTestCase struct {
	Variable       string
	ExpectedOutput string
}

func TestExtractMethodFromVariable(t *testing.T) {
	var testCases = []ExtractMethodFromVariableTestCase{
		{
			Variable:       "%cd?p=1&n=n%",
			ExpectedOutput: "cd",
		},
		{
			Variable:       "%cde?p=2&n=u%",
			ExpectedOutput: "cde",
		},
		{
			Variable:       "%cn?p=3&n=c%",
			ExpectedOutput: "cn",
		},
		{
			Variable:       "%asd?p=1&n=u%",
			ExpectedOutput: "asd",
		},
	}
	for _, testCase := range testCases {
		t.Run("Extract method from variable", func(t *testing.T) {
			output := variableParser.extractMethodFromVariable(testCase.Variable)
			if output != testCase.ExpectedOutput {
				t.Errorf("\n expected output => %s\n actual output => %s", testCase.ExpectedOutput, output)
			}
		})
	}
}

type ExtractParametersFromVariableTestCase struct {
	Variable         string
	ExpectedPosition string
	ExpectedNode     string
}

func TestExtractParametersFromVariable(t *testing.T) {
	var testCases = []ExtractParametersFromVariableTestCase{
		{
			Variable:         "%cd?p=1&n=n%",
			ExpectedPosition: "1",
			ExpectedNode:     "n",
		},
		{
			Variable:         "%cn?p=2&n=c%",
			ExpectedPosition: "2",
			ExpectedNode:     "c",
		},
		{
			Variable:         "%cde?p=3&n=u%",
			ExpectedPosition: "3",
			ExpectedNode:     "u",
		},
		{
			Variable:         "%cd?n=y&p=asd%",
			ExpectedPosition: "asd",
			ExpectedNode:     "y",
		},
	}
	for _, testCase := range testCases {
		t.Run("Extract parameters from variable", func(t *testing.T) {
			position, node := variableParser.extractParametersFromVariable(testCase.Variable)
			if position != testCase.ExpectedPosition {
				t.Errorf("Wrong position returned:\n expected position => %s\n actual position => %s", testCase.ExpectedPosition, position)
			}
			if node != testCase.ExpectedNode {
				t.Errorf("Wrong node returned:\n expected node => %s\n actual node => %s", testCase.ExpectedNode, node)
			}
		})
	}
}

type VariableParserTestCase struct {
	Title          string
	Input          string
	ExpectedOutput string
}

func TestVariableParser(t *testing.T) {
	var testCases = []VariableParserTestCase{
		{
			Title:          "text without variables should not be changed",
			Input:          "To jest tekst bez zadnych zmiennych",
			ExpectedOutput: "To jest tekst bez zadnych zmiennych",
		},
		{
			Title:          "should parse voucher value",
			Input:          "Odbierz bon na wartość " + voucherValueKey + " zł.",
			ExpectedOutput: "Odbierz bon na wartość {voucher_value} zł.",
		},
		{
			Title:          "should parse campaign discount",
			Input:          "Promocja w kampanii: " + topCampaignDiscountKey + "p=1&n=c%",
			ExpectedOutput: "Promocja w kampanii: {discount?pos=1&type=current}",
		},
		{
			Title:          "should parse voucher min order value",
			Input:          "Minimalna wartość zamówienia " + voucherMinOrderValueKey + " zł.",
			ExpectedOutput: "Minimalna wartość zamówienia {minimal_order_value} zł.",
		},
		{
			Title:          "should parse voucher code",
			Input:          "Kod bonu to " + voucherCodeKey,
			ExpectedOutput: "Kod bonu to {voucher_code}",
		},
		{
			Title:          "should parse voucher expiration date",
			Input:          "Bon jest ważny do " + voucherExpirationKey,
			ExpectedOutput: "Bon jest ważny do {expiration_date}",
		},
		{
			Title:          "should parse voucher expiration date natural",
			Input:          "Bon jest ważny do " + voucherExpirationNaturalKey,
			ExpectedOutput: "Bon jest ważny do {expiration_date_natural}",
		},
		{
			Title:          "should parse voucher expiration date natural accusative",
			Input:          "Bon jest ważny do " + voucherExpirationNaturalAccusativeKey,
			ExpectedOutput: "Bon jest ważny do {expiration_date_natural_accusative}",
		},
		{
			Title:          "should parse voucher limit",
			Input:          "Uwaga! Limit bonów wynosi " + voucherLimitKey + " i nie będzie rozszerzany.",
			ExpectedOutput: "Uwaga! Limit bonów wynosi {voucher_count_limit} i nie będzie rozszerzany.",
		},
		{
			Title:          "should parse same variable twice",
			Input:          "Bon na wartość " + voucherValueKey + ". Powtarzam: wartość " + voucherValueKey + ".",
			ExpectedOutput: "Bon na wartość {voucher_value}. Powtarzam: wartość {voucher_value}.",
		},
		{
			Title:          "should parse multiple occurences of variables",
			Input:          "Bon na wartość " + voucherValueKey + " ważny jest do " + voucherExpirationKey + ". Wartość: " + voucherValueKey + ", data: " + voucherExpirationKey + ".",
			ExpectedOutput: "Bon na wartość {voucher_value} ważny jest do {expiration_date}. Wartość: {voucher_value}, data: {expiration_date}.",
		},
		{
			Title:          "should not parse when no variables but percentage character occurs",
			Input:          "To jest tekst bez zmiennych, ale ma % dla zmyłki i jeszcze jeden % nawet.",
			ExpectedOutput: "To jest tekst bez zmiennych, ale ma % dla zmyłki i jeszcze jeden % nawet.",
		},
		{
			Title:          "should parse two complex variables with different parameters",
			Input:          "Nazwa kampanii pierwszej: " + topCampaignName + "p=1&n=u%" + ", kampanii drugiej: " + topCampaignName + "p=2&n=n%",
			ExpectedOutput: "Nazwa kampanii pierwszej: {campaign_name?pos=1&type=upcoming}, kampanii drugiej: {campaign_name?pos=2&type=now}",
		},
		{
			Title:          "should parse text containing polish diacritics",
			Input:          "Oto żółć: " + topCampaignName + "p=1&n=n%" + ", i również tu: " + topCampaignName + "p=2&n=n%",
			ExpectedOutput: "Oto żółć: {campaign_name?pos=1&type=now}, i również tu: {campaign_name?pos=2&type=now}",
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.Title, func(t *testing.T) {
			output := variableParser.Parse(testCase.Input)
			if output != testCase.ExpectedOutput {
				t.Errorf("\n expected output => %s\n actual output => %s", testCase.ExpectedOutput, output)
			}
		})
	}
}
