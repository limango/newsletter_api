package variable_parser

import (
	"regexp"
	"strconv"
	"strings"
	"time"
	"unicode/utf8"

	"bitbucket.org/limango/newsletter_api/campaign"

	"github.com/donatj/parsestr"
	"github.com/huandu/xstrings"
)

// these are simple placeholders for variables:
const voucherValueKey = "%vvalue%"
const voucherMinOrderValueKey = "%vmov%"
const voucherCodeKey = "%vcode%"
const voucherLimitKey = "%vlimit%"
const voucherExpirationKey = "%vexpiration_1%"
const voucherExpirationBKey = "%vexpiration_1b%"
const voucherLifetimeKey = "%vexpiration_3%"
const voucherExpirationNaturalKey = "%vexpiration_2a%"
const voucherExpirationNaturalAccusativeKey = "%vexpiration_2b%"

// these all placeholders take parameters after "?": pos and type
// e.g. %cn?p=2&n=n%
const topCampaignName = "%cn?"
const topCampaignDiscountKey = "%cd?"
const topCampaignDescriptionKey = "%cde?"

type VariableParser interface {
	Parse(text string) string
	ContainsPlaceholder(text string) bool
}

type VariableParserImpl struct {
	variablesFetcher      VariableFetcher
	simpleVariablesMapper map[string]string
}

func NewVariableParser(provider *campaign.CampaignProvider, shipmentTime time.Time, voucherId int) VariableParser {
	return VariableParserImpl{
		variablesFetcher: NewVariablesFetcher(*provider, shipmentTime, voucherId),
	}
}

func (vp VariableParserImpl) Parse(text string) string {
	if len(text) == 0 {
		return text
	}
	if !vp.ContainsPlaceholder(text) {
		return text
	}

	// fix for html entities encoded in some forms
	text = strings.Replace(text, "&amp", "&", -1)

	for key, val := range vp.getSimpleVariablesMapper() {
		text = strings.Replace(text, key, val, -1)
	}

	text = vp.parseComplexVariables(text)
	return text
}

func (vp VariableParserImpl) ContainsPlaceholder(text string) bool {
	for _, val := range getAvailableKeys() {
		if strings.Index(text, val) != -1 {
			return true
		}
	}
	return false
}

func getAvailableKeys() []string {
	return []string{
		voucherValueKey,
		voucherMinOrderValueKey,
		voucherCodeKey,
		voucherLimitKey,
		voucherExpirationKey,
		voucherExpirationBKey,
		voucherLifetimeKey,
		voucherExpirationNaturalKey,
		voucherExpirationNaturalAccusativeKey,

		// these are complex variables
		topCampaignName,
		topCampaignDiscountKey,
		topCampaignDescriptionKey,
	}
}

func (vp VariableParserImpl) getSimpleVariablesMapper() map[string]string {
	if vp.simpleVariablesMapper == nil {
		vp.simpleVariablesMapper = make(map[string]string)
		vp.simpleVariablesMapper[voucherValueKey] = vp.variablesFetcher.GetVoucherValue()
		vp.simpleVariablesMapper[voucherMinOrderValueKey] = vp.variablesFetcher.GetMinimalOrderValueForVoucher()
		vp.simpleVariablesMapper[voucherCodeKey] = vp.variablesFetcher.GetVoucherCode()
		vp.simpleVariablesMapper[voucherLimitKey] = vp.variablesFetcher.GetVoucherCountLimit()
		vp.simpleVariablesMapper[voucherExpirationKey] = vp.variablesFetcher.GetVoucherExpirationDate()
		vp.simpleVariablesMapper[voucherExpirationNaturalKey] = vp.variablesFetcher.GetVoucherExpirationDateNaturalLang()
		vp.simpleVariablesMapper[voucherExpirationNaturalAccusativeKey] = vp.variablesFetcher.GetVoucherExpirationDateNaturalLangAccusative()
	}
	return vp.simpleVariablesMapper
}

func (vp VariableParserImpl) parseComplexVariables(text string) string {
	complexVarRegexp := regexp.MustCompile("(%c)(.*?)(%)")
	complexVars := complexVarRegexp.FindAllString(text, -1)
	for _, complexVar := range complexVars {
		text = strings.Replace(text, complexVar, vp.getComplexVariableValue(complexVar), -1)
	}
	return text
}

// getAllPositionsOfString returns slice of positions of substr in text
// positions are counted for utf-8 runes, so polish diacritics are counted as 1 instead of e.g. 2
// e.g. getAllPositionsOfString("jakieś coś", "coś") = []int{7}
func (vp VariableParserImpl) getAllPositionsOfString(text, substr string) []int {
	positions := []int{}
	adjustment := 0

	for pos := vp.mbStrPos(text, substr); pos != -1; pos = vp.mbStrPos(text, substr) {
		positions = append(positions, pos+adjustment)
		adjustment += pos + utf8.RuneCountInString(substr)

		charPos := strings.Index(text, substr) + len(substr)
		text = text[charPos:]
	}

	return positions
}

// mbStrPos returns position of substring in runes instead of bytes
func (vp VariableParserImpl) mbStrPos(str, substr string) int {
	index := strings.Index(str, substr)
	if index == -1 {
		return -1
	}
	return utf8.RuneCountInString(str[:index])
}

func (vp VariableParserImpl) extractMethodFromVariable(variable string) string {
	return xstrings.Slice(variable, vp.mbStrPos(variable, "%")+1, vp.mbStrPos(variable, "?"))
}

// extractParametersFromVariable returns values of parameters p (position) and n (node) from parsed variable
func (vp VariableParserImpl) extractParametersFromVariable(variable string) (string, string) {
	parametersString := xstrings.Slice(variable, vp.mbStrPos(variable, "?")+1, len(variable)-1)
	result, err := parsestr.ParseQuery(parametersString)
	if err != nil {
		return "", ""
	}

	return result["p"][0], result["n"][0]
}

// getComplexVariableValue returns value specified by method, position and node of campaigns in ns
func (vp VariableParserImpl) getComplexVariableValue(variable string) string {

	method := vp.extractMethodFromVariable(variable)
	position, node := vp.extractParametersFromVariable(variable)

	p, err := strconv.Atoi(position)
	if err != nil || p < 1 {
		p = 1
	}
	node = vp.convertCampaignTypeParameter(node)

	switch method {
	case "cn":
		return vp.variablesFetcher.GetCampaignName(p, node)
	case "cde":
		return vp.variablesFetcher.GetCampaignDescription(p, node)
	case "cd":
		return vp.variablesFetcher.GetCampaignDiscount(p, node)
	default:
		return ""
	}
}

// convertCampaignTypeParameter
func (vp VariableParserImpl) convertCampaignTypeParameter(node string) string {
	switch node {
	case "n":
		return "now"
	case "u":
		return "upcoming"
	case "c":
		return "current"
	default:
		return "now"
	}
}
