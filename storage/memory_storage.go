package storage

type MemoryStorage struct {
	data map[string][]byte
}

func (s *MemoryStorage) Set(key string, value []byte) error {
	s.data[key] = value
	return nil
}

func (s *MemoryStorage) Get(key string) ([]byte, error) {
	if val, ok := s.data[key]; ok {
		return val, nil
	}
	return nil, ItemNotFoundError
}

func NewMemoryStorage() *MemoryStorage {
	return &MemoryStorage{
		data: make(map[string][]byte),
	}
}
