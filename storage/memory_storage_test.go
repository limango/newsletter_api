package storage

import (
	"bytes"
	"testing"
)

const key = "123"
const value = "some value"

func TestMemoryStorage(t *testing.T) {

	mem := NewMemoryStorage()

	if val, err := mem.Get(key); err != ItemNotFoundError || val != nil {
		t.Errorf("should be empty")
	}

	if err := mem.Set(key, Value(value)); err != nil {
		t.Errorf("error should be empty")
	}

	if _, err := mem.Get(key); err != nil {
		t.Errorf("should not return error: %v", err)
	}

	if val, _ := mem.Get(key); val == nil {
		t.Errorf("should not return nil value")
	}

	if val, _ := mem.Get(key); !bytes.Equal(val, []byte(value)) {
		t.Errorf("should return correct value: %v", val)
	}
}
