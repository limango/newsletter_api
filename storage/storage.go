package storage

import "errors"

// Storage is an interface over data storage that contains newsletters
// Set method takes key and value, and returns error if setting for some reason fails
// Get method takes key and returns pointer to string value and error if data couldn't be retrieved
// If there was no value under specified key, Get returns ItemNotFoundError
type Storage interface {
	Set(key string, value []byte) error
	Get(key string) ([]byte, error)
}

var ItemNotFoundError = errors.New("item not found in storage")
var FailedToFetchData = errors.New("failed to fetch data from storage")
