package campaign

import (
	"errors"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/joho/godotenv"
)

var campaignsJson = `
{
  "campaigns": [
    {
      "id": 39540,
      "name": "Risk made in Warsaw",
      "freetext": "odzież damska",
      "active": true,
      "start": "2018-12-07T10:00:00+01:00",
      "end": "2018-12-10T23:59:59+01:00",
      "branch": {
        "Women": true
      },
      "flags": {
        
      },
      "position": 1,
      "category": {
        "id": 7,
        "name": "Odzież",
        "main_name": "Odzież"
      },
      "outlet": false,
      "description": "Moda dla wyjątkowych kobiet",
      "long_description": "Risk Made in Warsaw to ubrania tworzone dla kobiet przez kobiety. Polskim projektantkom przyświeca myśl, że nie ma złych sylwetek – są tylko nieodpowiednio skrojone ubrania. Dlatego wśród modnych propozycji marki znajdziemy wygodne cygaretki, eleganckie spódnice, klasyczne trencze i wiele innych ubrań, które pozwolą Ci czuć się i wyglądać kobieco każdego dnia. ",
      "img_logo": "//img.limango.de/media/pl/39540/li20_logo.jpg",
      "img_shop": "//img.limango.de/media/pl/39540/30_ihp_quadrat.jpg",
      "discount": 42,
      "shipping_start": "2018-12-21T00:00:00+01:00",
      "shipping_end": "2019-01-05T00:00:00+01:00",
      "shipping_text": "21.12.2018-05.01.2019",
      "orders_count": 0,
      "views_count": 93,
      "last_updated": "2018-12-07T09:58:39.060571593+01:00"
    },
    {
      "id": 41240,
      "name": "Perfumy",
      "freetext": "dla niej i dla niego",
      "active": true,
      "start": "2018-12-07T07:00:00+01:00",
      "end": "2018-12-09T23:59:59+01:00",
      "branch": {
        "Men": true,
        "Women": true
      },
      "flags": {
        "Christmas": true
      },
      "position": 2,
      "category": {
        "id": 114,
        "name": "Uroda i pielęgnacja",
        "main_name": "Kosmetyki"
      },
      "outlet": false,
      "description": "Wejdź do świata zapachów!",
      "long_description": "Kilka kropel ulubionych perfum może poprawić nastrój i dodać pewności siebie. Niezależnie od tego, czy masz już zapach, który idealnie do Ciebie pasuje, czy też dopiero szukasz odpowiedniej kompozycji, teraz na limango znajdziesz najlepsze zapachy znanych i uwielbianych na całym świecie marek: DKNY, Calvin Klein, Lacoste, Versace i wielu innych.",
      "img_logo": "//img.limango.de/media/pl/41240/li20_logo.jpg",
      "img_shop": "//img.limango.de/media/pl/41240/30_ihp_quadrat.jpg",
      "discount": 51,
      "shipping_start": "2018-12-10T00:00:00+01:00",
      "shipping_end": "2018-12-21T00:00:00+01:00",
      "shipping_text": "10.12.2018-21.12.2018",
      "orders_count": 18,
      "views_count": 2735,
      "last_updated": "2018-12-07T09:59:34.174578822+01:00"
    },
    {
      "id": 41420,
      "name": "Estée Lauder",
      "freetext": "kosmetyki",
      "active": true,
      "start": "2018-12-07T07:00:00+01:00",
      "end": "2018-12-11T23:59:59+01:00",
      "branch": {
        "Women": true
      },
      "flags": {
        
      },
      "position": 3,
      "category": {
        "id": 114,
        "name": "Uroda i pielęgnacja",
        "main_name": "Kosmetyki"
      },
      "outlet": false,
      "description": "Podaruj sobie odrobinę luksusu!",
      "long_description": "Estée Lauder to firma, która od przeszło 60 lat zmienia oblicze przemysłu kosmetycznego, zgodnie z przekonaniem założycielki, iż każda kobieta może być piękna. W naszej kampanii znajdziecie wszystko, czego potrzeba do wykonania perfekcyjnego makijażu.",
      "img_logo": "//img.limango.de/media/pl/41420/li20_logo.jpg",
      "img_shop": "//img.limango.de/media/pl/41420/30_ihp_quadrat.jpg",
      "discount": 32,
      "shipping_start": "2018-12-11T00:00:00+01:00",
      "shipping_end": "2018-12-22T00:00:00+01:00",
      "shipping_text": "11.12.2018-22.12.2018",
      "orders_count": 12,
      "views_count": 2439,
      "last_updated": "2018-12-07T09:59:41.448297046+01:00"
    },
    {
      "id": 25891,
      "name": "Stylowy piątek",
      "freetext": "torebki",
      "active": true,
      "start": "2018-12-07T00:00:00+01:00",
      "end": "2018-12-07T23:59:59+01:00",
      "branch": {
        "Women": true
      },
      "flags": {
        "Ending": true,
        "FastDelivery": true
      },
      "position": 6,
      "category": {
        "id": 10,
        "name": "Outdoor i sport",
        "main_name": "Odzież"
      },
      "outlet": true,
      "link": "https://www.limango-outlet.pl/torebki-damskie?tree=damskie-torby-na-ramie&campaign=2_25891",
      "description": "torebki",
      "long_description": "torebki",
      "img_logo": "",
      "img_shop": "//img.limango.de/media/outlet/campaigns/25891/30_ihp_quadrat.jpg",
      "discount": 85,
      "shipping_start": "2016-11-29T00:00:00+01:00",
      "shipping_end": "2016-12-10T00:00:00+01:00",
      "shipping_text": "Dostawa w ciągu 2 - 3 dni roboczych",
      "orders_count": 0,
      "views_count": 8103,
      "last_updated": "2018-12-07T09:57:58.155501506+01:00"
    },
    {
      "id": 25890,
      "name": "Stylowy piątek",
      "freetext": "czółenka",
      "active": true,
      "start": "2018-12-07T00:00:00+01:00",
      "end": "2018-12-07T23:59:59+01:00",
      "branch": {
        "Women": true
      },
      "flags": {
        "Ending": true,
        "FastDelivery": true
      },
      "position": 5,
      "category": {
        "id": 10,
        "name": "Outdoor i sport",
        "main_name": "Odzież"
      },
      "outlet": true,
      "link": "https://www.limango-outlet.pl/czolenka-damskie?campaign=2_25890",
      "description": "czółenka",
      "long_description": "czółenka",
      "img_logo": "",
      "img_shop": "//img.limango.de/media/outlet/campaigns/25890/30_ihp_quadrat.jpg",
      "discount": 85,
      "shipping_start": "2016-12-03T00:00:00+01:00",
      "shipping_end": "2016-12-15T00:00:00+01:00",
      "shipping_text": "Dostawa w ciągu 2 - 3 dni roboczych",
      "orders_count": 0,
      "views_count": 2,
      "last_updated": "2018-12-07T09:30:03.435792046+01:00"
    },
    {
      "id": 35569,
      "name": "Morphy Richards",
      "freetext": "sprzęt AGD",
      "active": true,
      "start": "2018-12-07T19:00:00+01:00",
      "end": "2018-12-17T23:59:59+01:00",
      "branch": {
        "HomeLiving": true
      },
      "flags": {
        "Christmas": true,
        "FastDelivery": true
      },
      "position": 1,
      "category": {
        "id": 13,
        "name": "Technika",
        "main_name": "Technika"
      },
      "outlet": false,
      "description": "Praktyczny sprzęt do Twojego domu",
      "long_description": "Morphy Richards to marka produkująca najwyższej jakości sprzęt domowy. Misją Morphy Richards jest projektowanie prostych, użytecznych i estetycznie doskonałych produktów dla Twojego gospodarstwa domowego. Konsekwentnie najwyższa jakość i technologia, pozwalają zajmować im od wielu lat czołowe miejsce wśród producentów artykułów dla gospodarstw domowych. Zapraszamy!",
      "img_logo": "//img.limango.de/media/pl/35569/li20_logo.jpg",
      "img_shop": "//img.limango.de/media/pl/35569/30_ihp_quadrat.jpg",
      "discount": 52,
      "shipping_start": "2018-12-17T00:00:00+01:00",
      "shipping_end": "2018-12-20T00:00:00+01:00",
      "shipping_text": "17.12.2018-20.12.2018",
      "campaign_extra": {
        "fast_delivery_info": "Prosimy o przemyślane zakupy, ponieważ w przypadku kampanii z szybką wysyłką anulowanie zamówień jest niemożliwe."
      },
      "orders_count": 3,
      "views_count": 1795,
      "last_updated": "2018-12-13T09:29:40.112291641+01:00"
    },
    {
      "id": 39414,
      "name": "Villa d´Este",
      "freetext": "naczynia",
      "active": true,
      "start": "2018-12-08T07:00:00+01:00",
      "end": "2018-12-13T23:59:59+01:00",
      "branch": {
        "HomeLiving": true
      },
      "flags": {
        
      },
      "position": 7,
      "category": {
        "id": 8,
        "name": "Dom",
        "main_name": "Dom"
      },
      "outlet": false,
      "description": "Włoski design w Twojej kuchni",
      "long_description": "Ponadczasowy wygląd, bogata kolorystyka i eleganckie wykończenie – te cechy doskonale opisują produkty Villa d´Este. Wśród produktów tej włoskiej marki znajdziesz oryginalną zastawę stołową z wysokiej jakości porcelany i nie tylko, a do tego artykuły dekoracyjne, które uzupełnią aranżację Twojej kuchni czy jadalni. Villa d´Este to rozwiązanie dla miłośników niebanalnych kolorów i stylowego wzornictwa.",
      "img_logo": "//img.limango.de/media/pl/39414/li20_logo.jpg",
      "img_shop": "//img.limango.de/media/pl/39414/30_ihp_quadrat.jpg",
      "discount": 63,
      "shipping_start": "2019-01-07T00:00:00+01:00",
      "shipping_end": "2019-01-18T00:00:00+01:00",
      "shipping_text": "07.01.2019-18.01.2019",
      "orders_count": 5,
      "views_count": 2199,
      "last_updated": "2018-12-07T09:59:27.397223674+01:00"
    },
    {
      "id": 40016,
      "name": "Manode",
      "freetext": "odzież damska",
      "active": true,
      "start": "2018-12-08T19:00:00+01:00",
      "end": "2018-12-16T23:59:59+01:00",
      "branch": {
        "Women": true
      },
      "flags": {
        
      },
      "position": 8,
      "category": {
        "id": 7,
        "name": "Odzież",
        "main_name": "Odzież"
      },
      "outlet": false,
      "description": "Z najlepszych materiałów",
      "long_description": "Manode to francuska marka, której projekty trafiają w gust kobiet w każdym wieku. Popularność zdobyła przede wszystkim dzięki materiałom, które wykorzystuje do tworzenia swoich ubrań – wśród nich znaleźć można między innymi kaszmir, wełnę czy wysokogatunkową bawełnę. Manode oferuje modne swetry w ponadczasowych krojach, komfortowe kardigany, a także idealne na chłodnie dni czapki.",
      "img_logo": "//img.limango.de/media/pl/40016/li20_logo.jpg",
      "img_shop": "//img.limango.de/media/pl/40016/30_ihp_quadrat.jpg",
      "discount": 71,
      "shipping_start": "2018-12-31T00:00:00+01:00",
      "shipping_end": "2019-01-12T00:00:00+01:00",
      "shipping_text": "31.12.2018-12.01.2019",
      "orders_count": 9,
      "views_count": 2079,
      "last_updated": "2018-12-07T09:59:47.212508864+01:00"
    }
]
}
`

var campaignProvider CampaignProvider

func setUp() {
	if err := godotenv.Load("../.env"); err != nil {
		fmt.Printf("Error when loading dotenv files: %s\n", err.Error())
		os.Exit(1)
	}

	campaignProvider = &CampaignProviderImpl{
		source: new(MockCampaignsSource),
	}
}

type MockCampaignsSource struct {
	campaigns []Campaign
}

func (cs *MockCampaignsSource) GetCampaigns() []Campaign {
	if cs.campaigns == nil {
		campaigns, err := GetCampaignsFromJson([]byte(campaignsJson))
		if err != nil {
			fmt.Printf("could not load campaigns from json: %s \n terminating", err)
			os.Exit(1)
		}
		cs.campaigns = campaigns
	}
	return cs.campaigns
}

type CampaignType uint8

const (
	Starting CampaignType = iota
	Current
	Upcoming
	All
)

type CampaignProviderTestCase struct {
	Title               string
	CType               CampaignType
	InputTimeString     string
	ExpectedCampaignIds []int
}

func TestCampaignProviderImpl(t *testing.T) {

	testInputs := []CampaignProviderTestCase{
		{
			Title:               "all campaigns should return, surprisingly, all campaigns",
			CType:               All,
			InputTimeString:     "2018-12-08T00:00:00+01:00",
			ExpectedCampaignIds: []int{39540, 41240, 41420, 25891, 25890, 35569, 39414, 40016},
		},
		{
			Title:               "far future date shouldn't return any current campaigns",
			CType:               Current,
			InputTimeString:     "2020-12-08T00:00:00+01:00",
			ExpectedCampaignIds: []int{},
		},
		{
			Title:               "far future date shouldn't return any starting campaigns",
			CType:               Starting,
			InputTimeString:     "2020-12-08T00:00:00+01:00",
			ExpectedCampaignIds: []int{},
		},
		{
			Title:               "far past should return all campaigns for upcoming",
			CType:               Upcoming,
			InputTimeString:     "2017-12-08T00:00:00+01:00",
			ExpectedCampaignIds: []int{39540, 41240, 41420, 25891, 25890, 35569, 39414, 40016},
		},
		{
			Title:               "starting 07.12 07:00:00 should return 4 campaigns",
			CType:               Starting,
			InputTimeString:     "2018-12-07T07:00:00+01:00",
			ExpectedCampaignIds: []int{41240, 41420, 25891, 25890},
		},
		{
			Title:               "starting 07.12 07:01:00 should return 2 campaigns",
			CType:               Starting,
			InputTimeString:     "2018-12-07T07:01:00+01:00",
			ExpectedCampaignIds: []int{25891, 25890},
		},
		{
			Title:               "starting 07.12 19:00:00 should return 1 campaign",
			CType:               Starting,
			InputTimeString:     "2018-12-07T19:00:00+01:00",
			ExpectedCampaignIds: []int{35569},
		},
		{
			Title:               "starting 08.12 07:00:00 should return 1 campaign",
			CType:               Starting,
			InputTimeString:     "2018-12-08T07:00:00+01:00",
			ExpectedCampaignIds: []int{39414},
		},
		{
			Title:               "upcoming 07.12 07:00:00 should return 4 campaigns",
			CType:               Upcoming,
			InputTimeString:     "2018-12-07T07:00:00+01:00",
			ExpectedCampaignIds: []int{39540, 40016, 39414, 35569},
		},
		{
			Title:               "upcoming 07.12 10:00:00 should return 3 campaigns",
			CType:               Upcoming,
			InputTimeString:     "2018-12-07T10:00:00+01:00",
			ExpectedCampaignIds: []int{40016, 39414, 35569},
		},
		{
			Title:               "upcoming 08.12 07:00:00 should return 1 campaign",
			CType:               Upcoming,
			InputTimeString:     "2018-12-08T07:00:00+01:00",
			ExpectedCampaignIds: []int{40016},
		},
		{
			Title:               "upcoming 08.12 19:00:00 should return 0 campaigns",
			CType:               Upcoming,
			InputTimeString:     "2018-12-08T19:00:00+01:00",
			ExpectedCampaignIds: []int{},
		},
		{
			Title:               "current 07.12 06:00:00 should return 2 campaigns",
			CType:               Current,
			InputTimeString:     "2018-12-07T06:00:00+01:00",
			ExpectedCampaignIds: []int{25891, 25890},
		},
		{
			Title:               "current 07.12 07:00:00 should return 0 campaigns",
			CType:               Current,
			InputTimeString:     "2018-12-07T07:00:00+01:00",
			ExpectedCampaignIds: []int{},
		},
		{
			Title:               "current 08.12 06:00:00 should return 4 campaigns",
			CType:               Current,
			InputTimeString:     "2018-12-08T06:00:00+01:00",
			ExpectedCampaignIds: []int{39540, 41240, 41420, 35569},
		},
		{
			Title:               "current 17.12 16:00:00 should return 1 campaigns",
			CType:               Current,
			InputTimeString:     "2018-12-17T16:00:00+01:00",
			ExpectedCampaignIds: []int{35569},
		},
		{
			Title:               "current 18.12 16:00:00 should return 0 campaigns",
			CType:               Current,
			InputTimeString:     "2018-12-18T16:00:00+01:00",
			ExpectedCampaignIds: []int{},
		},
	}

	setUp()
	for _, test := range testInputs {
		t.Run(test.Title, func(t *testing.T) {

			inputTime, err := time.Parse(time.RFC3339, test.InputTimeString)
			if err != nil {
				t.Errorf("\nfailed to parse time string: %s\n", err)
			}

			campaigns := getCampaignOfType(test.CType, inputTime)

			if len(campaigns) != len(test.ExpectedCampaignIds) {
				t.Errorf("\ncampaign provider returned incorrect number of campaigns:\n expected: %d\n returned: %d\n", len(test.ExpectedCampaignIds), len(campaigns))
			}

			for _, cid := range test.ExpectedCampaignIds {
				if !existsCampaignOfCid(campaigns, cid) {
					t.Errorf("\nreturned campaigns were expected to have campaign with cid = %d\n", cid)
				}
			}

			for _, campaign := range campaigns {
				if err := checkCorrectCampaignTimeInRelationToNewsletterTime(&campaign, inputTime, test.CType); err != nil {
					t.Errorf("\ncampaign provider returned incorrect campaign for given type. \n error => %s", err)
				}
			}
		})
	}
}

func getCampaignOfType(campaignType CampaignType, inputTime time.Time) []Campaign {
	switch campaignType {
	case Current:
		return campaignProvider.GetCurrentCampaigns(inputTime)
	case Starting:
		return campaignProvider.GetStartingCampaigns(inputTime)
	case Upcoming:
		return campaignProvider.GetUpcomingCampaigns(inputTime)
	case All:
		return campaignProvider.GetAllCampaigns()
	}
	return nil
}

func existsCampaignOfCid(campaigns []Campaign, cid int) bool {
	for _, campaign := range campaigns {
		if campaign.Id == cid {
			return true
		}
	}
	return false
}

func checkCorrectCampaignTimeInRelationToNewsletterTime(campaign *Campaign, inputTime time.Time, campaignType CampaignType) error {

	switch campaignType {
	case All:
		return nil
	case Current:
		isCurrent := campaign.Start.Before(inputTime) && campaign.End.After(inputTime)
		if !isCurrent {
			return errors.New(fmt.Sprintf("campaign should not be among current.\n start: %s\n end: %s\n newsletter time: %s", campaign.Start, campaign.End, inputTime))
		}
	case Upcoming:
		isUpcoming := campaign.Start.After(inputTime)
		if !isUpcoming {
			return errors.New(fmt.Sprintf("campaign should not be amoung upcoming.\n start: %s\n newsletter time: %s", campaign.Start, inputTime))
		}
	case Starting:
		isStarting := campaign.Start.Equal(inputTime) || IsStartingOutlet(*campaign, inputTime)
		if !isStarting {
			return errors.New(fmt.Sprintf("campaign should not be among starting.\n start: %s\n newsletter time: %s", campaign.Start, inputTime))
		}
	}
	return nil
}
