package campaign

import (
	"encoding/xml"
	"fmt"
	"net/http"
	"strconv"
	"time"
)

const CampaignUrlStart = "https://www.limango.pl/g.php?H=%accesscode%&ref=shop/overview&cid="

type Campaign struct {
	Core
	Description            string    `json:"description" xml:"short_desc"`
	LongDescription        string    `json:"long_description"`
	ImgLogo                string    `json:"img_logo"`
	ImgShop                string    `json:"img_shop"`
	Discount               int       `json:"discount"`
	ShippingStart          time.Time `json:"shipping_start"`
	ShippingEnd            time.Time `json:"shipping_end"`
	ShippingText           string    `json:"shipping_text"`
	OrdersCount            int       `json:"orders_count"`
	ViewsCount             int       `json:"views_count"`
	NewsletterShippingDate time.Time `json:"-" xml:"-"`
	NewsletterNode         string    `json:"-" xml:"-"`
	NewsletterGroupID      int       `json:"-" xml:"-"`
	Url                    string    `json:"-" xml:"-"`
	CustomImageUrl         string    `json:"-" xml:"-"`
	standardImageUrl       string
}

type Core struct {
	Id       int              `json:"id" xml:"campaign_id"`
	Name     string           `json:"name" xml:"name"`
	Freetext string           `json:"freetext"`
	Active   bool             `json:"active"`
	Start    time.Time        `json:"start"`
	End      time.Time        `json:"end"`
	Branch   map[string]bool  `json:"branch" xml:"-"`
	Flags    map[string]bool  `json:"flags" xml:"-"`
	Position int              `json:"position"`
	Category CampaignCategory `json:"category" xml:"campaign_category"`
	Outlet   bool             `json:"outlet"`
	Link     string           `json:"link,omitempty"`
	Score    float32          `json:"score,omitempty"`
}

type CampaignCategory struct {
	Id       int    `db:"cid_categoryID" json:"id"`
	Name     string `db:"cid_categoryNamePl" json:"name"`
	MainName string `db:"cid_mainCategoryNamePl" json:"main_name"`
}

func (c *Campaign) GetConversion() float32 {
	if c.ViewsCount > 0 {
		return float32((float32(c.OrdersCount) / float32(c.ViewsCount)) * 100)
	}
	return float32(0)
}

func (c *Campaign) GetUrl() string {
	if len(c.Url) == 0 {
		c.Url = CampaignUrlStart + strconv.Itoa(c.Id) + "&utm_source=limango&utm_medium=newsletter&utm_campaign=" + c.NewsletterShippingDate.Format("20060102") + "&utm_content=" + strconv.Itoa(c.Id) + "_" + c.NewsletterNode + "&utm_term=%mailing%"
	}
	return c.Url
}

func (c *Campaign) GetCategory() string {
	if c.Branch["Kids"] == true || c.Branch["Baby"] == true {
		return "kids"
	}
	return "non-kids"
}

func (c *Campaign) GetImageUrl() string {
	if c.IsCustomImageSet() {
		return c.GetCustomImageUrl()
	}
	return c.GetStandardImageUrl()
}

func (c *Campaign) IsCustomImageSet() bool {
	return isUrlResponding(c.GetCustomImageUrl())
}

// bedzie trzeba napisac do tego jakies testy
func (c *Campaign) GetCustomImageUrl() string {
	if c.CustomImageUrl == "" {
		url := "https://cdn.limango.pl/images/sare_newsletter/" + strconv.Itoa(c.NewsletterGroupID) + "_" + strconv.Itoa(c.Id) + ".jpg"
		if isUrlResponding(url) {
			c.CustomImageUrl = url
		}
	}
	return c.CustomImageUrl
}

func (c *Campaign) GetStandardImageUrl() string {
	if c.standardImageUrl == "" {
		url := c.generateStandardImageUrl()
		if isUrlResponding(url) {
			c.standardImageUrl = url
		} else {
			return "https:" + c.ImgShop
		}
	}
	return c.standardImageUrl
}

func (c *Campaign) generateStandardImageUrl() string {
	versionNumber := time.Now().Format("20060102")
	if c.Outlet {
		return "https://limango-res.cloudinary.com/t_newsletter/v" + versionNumber + "/media/newsletter/campaigns/outlet/" + strconv.Itoa(c.Id) + "/pl_tile_desktopNormal.jpg"
	} else {
		return "https://limango-res.cloudinary.com/t_newsletter/v" + versionNumber + "/media/newsletter/campaigns/privateshop/" + strconv.Itoa(c.Id) + "/pl_tile_desktopNormal.jpg"
	}
}

// lazy way of checking, probably should do it somehow better
func isUrlResponding(url string) bool {
	client := &http.Client{
		Timeout: 2 * time.Second,
	}
	resp, err := client.Get(url)
	return err == nil && resp.StatusCode == 200
}

type CampaignXMLRepresenation struct {
	XMLName          xml.Name `xml:"campaign"`
	CampaignCategory CData    `xml:"campaign_category"`
	CampaignID       int      `xml:"campaign_id"`
	Name             CData    `xml:"name"`
	ShortDesc        CData    `xml:"short_desc"`
	Position         int      `xml:"position"`
	DateStart        string   `xml:"date_start"`
	Conversion       string   `xml:"conversion"`
	Url              CData    `xml:"url"` // dodać testy newslettera sprawdzające poprawność Url w kampaniach
	Category         string   `xml:"category"`
	Image            CData    `xml:"image"`
}

type CData struct {
	Value string `xml:",cdata"`
}

func (c *Campaign) MarshalXML(e *xml.Encoder, start xml.StartElement) error {
	return e.Encode(c.createXmlRepresentation())
}

func (c *Campaign) createXmlRepresentation() CampaignXMLRepresenation {
	return CampaignXMLRepresenation{
		CampaignCategory: CData{c.Freetext},
		CampaignID:       c.Id,
		Name:             CData{c.Name},
		ShortDesc:        CData{c.Description},
		Position:         c.Position,
		DateStart:        c.Start.Format("2006-01-02"),
		Conversion:       fmt.Sprintf("%.2f", c.GetConversion()),
		Url:              CData{c.GetUrl()},
		Category:         c.GetCategory(),
		Image:            CData{c.GetImageUrl()},
	}
}
