package campaign

type ByConversion []Campaign

func (campaigns ByConversion) Len() int {
	return len(campaigns)
}

func (campaigns ByConversion) Swap(i, j int) {
	campaigns[i], campaigns[j] = campaigns[j], campaigns[i]
}

func (campaigns ByConversion) Less(i, j int) bool {
	return campaigns[i].GetConversion() < campaigns[j].GetConversion()
}

type ByPosition []Campaign

func (campaigns ByPosition) Len() int {
	return len(campaigns)
}

func (campaigns ByPosition) Swap(i, j int) {
	campaigns[i], campaigns[j] = campaigns[j], campaigns[i]
}

func (campaigns ByPosition) Less(i, j int) bool {
	if campaigns[i].Start.Format("2006-01-02") > campaigns[j].Start.Format("2006-01-02") {
		return true
	} else if campaigns[i].Start.Format("2006-01-02") < campaigns[j].Start.Format("2006-01-02") {
		return false
	}
	return campaigns[i].Position < campaigns[j].Position
}
