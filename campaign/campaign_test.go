package campaign

import (
	"testing"
)

type CampaignCategoryTestCase struct {
	Title            string
	Campaign         Campaign
	ExpectedCategory string
}

func TestCampaignCategory(t *testing.T) {
	testCases := []CampaignCategoryTestCase{
		CampaignCategoryTestCase{
			Title:            "Campaign with no branch should be of type \"non-kids\"",
			Campaign:         Campaign{},
			ExpectedCategory: "non-kids",
		},
		CampaignCategoryTestCase{
			Title: "Campaign with branch \"Kids\" should be of type \"kids\"",
			Campaign: Campaign{
				Core: Core{
					Branch: map[string]bool{
						"Kids":  true,
						"Men":   true,
						"Women": true,
					},
				},
			},
			ExpectedCategory: "kids",
		},
		CampaignCategoryTestCase{
			Title: "Campaign with branch \"Baby\" should be of type \"kids\"",
			Campaign: Campaign{
				Core: Core{
					Branch: map[string]bool{
						"Men":   true,
						"Baby":  true,
						"Women": true,
					},
				},
			},
			ExpectedCategory: "kids",
		},
		CampaignCategoryTestCase{
			Title: "Campaign with no branch \"Kids\" nor \"Baby\" should be of type \"non-kids\"",
			Campaign: Campaign{
				Core: Core{
					Branch: map[string]bool{
						"Men":       true,
						"Women":     true,
						"Something": true,
					},
				},
			},
			ExpectedCategory: "non-kids",
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.Title, func(t *testing.T) {
			if testCase.Campaign.GetCategory() != testCase.ExpectedCategory {
				t.Errorf("Test campaign category failed: %s\n expected category => %s \n received category => %s", testCase.Title, testCase.ExpectedCategory, testCase.Campaign.GetCategory())
			}
		})
	}
}

type CampaignConversionTestCase struct {
	Title              string
	Campaign           Campaign
	ExpectedConversion float32
}

func TestCampaignConversion(t *testing.T) {

	testCases := []CampaignConversionTestCase{
		CampaignConversionTestCase{
			Title: "Campaign without views should have conversion = 0",
			Campaign: Campaign{
				OrdersCount: 12,
				ViewsCount:  0,
			},
			ExpectedConversion: float32(0),
		},
		CampaignConversionTestCase{
			Title: "Campaign with orders = 5 and views = 100 should have conversion = 5",
			Campaign: Campaign{
				OrdersCount: 5,
				ViewsCount:  100,
			},
			ExpectedConversion: float32(5),
		},
		CampaignConversionTestCase{
			Title: "Campaign with orders = 5 and orders = 1000 should have conversion = 0.5",
			Campaign: Campaign{
				OrdersCount: 5,
				ViewsCount:  1000,
			},
			ExpectedConversion: float32(0.5),
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.Title, func(t *testing.T) {
			if testCase.Campaign.GetConversion() != testCase.ExpectedConversion {
				t.Errorf("Test campaign conversion failed: %s\n expected conversion => %.2f \n received conversion => %.2f", testCase.Title, testCase.ExpectedConversion, testCase.Campaign.GetConversion())
			}
		})
	}
}
