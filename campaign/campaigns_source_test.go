package campaign

import (
	"testing"
)

func TestCampaignsSource(t *testing.T) {
	setUp()

	cs := CampaignSourceImpl{}
	campaigns := cs.GetCampaigns()

	if len(campaigns) == 0 {
		t.Errorf("Campaigns data source returned 0 campaigns. \n")
	}
}
