package campaign

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.org/limango/newsletter_api/logger"

	"bitbucket.org/limango/tools"
	"github.com/robfig/cron"
)

type CampaignsSource interface {
	GetCampaigns() []Campaign
}

type CampaignSourceImpl struct {
	ddsHost   string
	campaigns []Campaign
	client    *http.Client
}

func NewCampaignSource() *CampaignSourceImpl {
	source := &CampaignSourceImpl{}

	c := cron.New()
	c.AddFunc("@every 10m", func() {
		source.loadCampaigns()
		logger.Log("reloading campaigns", logger.Info)
	})
	c.Start()

	return source
}

// CampaignsJsonStruct is only needed for unmarshalling json with nested campaigns from DDS
type CampaignsJsonStruct struct {
	Campaigns []Campaign
}

func (cs *CampaignSourceImpl) GetCampaigns() []Campaign {
	if len(cs.campaigns) == 0 {
		cs.loadCampaigns()
	}
	return FilterCampaigns(cs.campaigns, func(campaign Campaign) bool {
		return campaign.Active
	})
}

func (cs *CampaignSourceImpl) loadCampaigns() {
	campaignsJson := cs.fetchCampaignsJson()
	campaigns, err := GetCampaignsFromJson(campaignsJson)
	if err != nil {
		fmt.Printf("could not load campaigns from json: %s", err)
	}
	cs.campaigns = campaigns
}

// GetCampaignsFromJson parses json to Campaign slice and returns error if sth goes wrong
func GetCampaignsFromJson(campaignsJson []byte) ([]Campaign, error) {
	var cjs CampaignsJsonStruct
	err := json.Unmarshal(campaignsJson, &cjs)
	if err != nil {
		return nil, err
	}
	return cjs.Campaigns, nil
}

func (cs *CampaignSourceImpl) fetchCampaignsJson() []byte {
	response, err := http.Get(cs.getDDSHost() + "/campaigns/")
	if err != nil {
		tools.HandleError(err)
	}
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	return body
}

// getDDSHost returns Url for data source API
func (cs *CampaignSourceImpl) getDDSHost() string {
	if len(cs.ddsHost) == 0 {
		cs.ddsHost = tools.Env("DDS_HOST", "http://dds.limango.pl:8005")
	}
	return cs.ddsHost
}
