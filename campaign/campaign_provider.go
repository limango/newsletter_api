package campaign

import (
	"time"
)

// Campaign provider is an interface for providing campaigns under specific criteria.
// The most common criteria is campaign type in regards of campaign start time.
type CampaignProvider interface {
	GetAllCampaigns() []Campaign               // all campaigns that provider has
	GetCurrentCampaigns(time.Time) []Campaign  // campaign.start <= time
	GetStartingCampaigns(time.Time) []Campaign // campaign.start == time
	GetUpcomingCampaigns(time.Time) []Campaign // campaign.start > time
}

func NewCampaignProvider() CampaignProvider {
	source := NewCampaignSource()
	return &CampaignProviderImpl{
		source: source,
	}
}

type CampaignProviderImpl struct {
	source    CampaignsSource
	campaigns []Campaign
}

func (cp *CampaignProviderImpl) GetAllCampaigns() []Campaign {
	return cp.source.GetCampaigns()
}

func (cp *CampaignProviderImpl) GetCurrentCampaigns(t time.Time) []Campaign {
	return FilterCampaigns(cp.source.GetCampaigns(), func(c Campaign) bool {
		return c.Start.Before(t) && c.End.After(t) && !IsStartingOutlet(c, t)
	})
}

func (cp *CampaignProviderImpl) GetUpcomingCampaigns(t time.Time) []Campaign {
	return FilterCampaigns(cp.source.GetCampaigns(), func(c Campaign) bool {
		return c.Start.After(t)
	})
}

func (cp *CampaignProviderImpl) GetStartingCampaigns(t time.Time) []Campaign {
	return FilterCampaigns(cp.source.GetCampaigns(), func(c Campaign) bool {
		return c.Start.Equal(t) || IsStartingOutlet(c, t)
	})
}

// FilterCampaigns filters campaign collection for elements fulfilling specified condition
func FilterCampaigns(campaigns []Campaign, test func(Campaign) bool) (resultSlice []Campaign) {
	for _, element := range campaigns {
		if test(element) {
			resultSlice = append(resultSlice, element)
		}
	}
	return resultSlice
}

func IsStartingOutlet(c Campaign, t time.Time) bool {
	return c.Start.Before(t) &&
		c.Outlet &&
		t.Hour() == 7 &&
		t.Format("2006-01-02") == c.Start.Format("2006-01-02")
}
