package main

// @title limango PL newsletter_api
// @version 0.1
// @description This is microservice for handling newsletter creations.
/*
   // @contact.name PL Dev team
   // @contact.url https://jira.limango.de/secure/RapidBoard.jspa?rapidView=162

   // @host api.limango.pl/newsletter_api
*/

import (
	"fmt"
	netHttp "net/http"
	"os"

	"bitbucket.org/limango/newsletter_api/logger"

	"bitbucket.org/limango/newsletter_api/storage"

	"github.com/joho/godotenv"

	"bitbucket.org/limango/newsletter_api/campaign"

	"bitbucket.org/limango/newsletter_api/http"
	"bitbucket.org/limango/newsletter_api/s3bucket"
	"bitbucket.org/limango/newsletter_api/service"
	"bitbucket.org/limango/tools"
)

func main() {
	setup()

	s3Storage := getStorage()
	campaignProvider := getCampaignProvider()
	s := getService(s3Storage, campaignProvider)
	server := getServer(&s)

	logger.Log("Application start", logger.Info)
	err := server.ListenAndServe()
	if err != nil {
		fmt.Printf("ListenAndServe error: %s\n", err.Error())
		os.Exit(1)
	}
}

func setup() {
	if err := godotenv.Load(); err != nil {
		fmt.Printf("Error when loading dotenv files: %s\n", err.Error())
		os.Exit(1)
	}
}

func getStorage() storage.Storage {
	return new(s3bucket.AwsS3Storage)
}

func getCampaignProvider() campaign.CampaignProvider {
	return campaign.NewCampaignProvider()
}

func getService(storage storage.Storage, campaignProvider campaign.CampaignProvider) service.NewsletterService {
	return service.NewService(service.ServiceOptions{
		Storage:          storage,
		Version:          "v.0.1",
		CampaignProvider: campaignProvider,
	})
}

func getServer(s *service.NewsletterService) *netHttp.Server {
	return http.NewServer(s, &http.ServerConfig{
		Port: tools.Env("PORT", ":8003"),
	})
}
