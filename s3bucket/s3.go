package s3bucket

import (
	"bytes"
	"fmt"
	"os"

	"bitbucket.org/limango/tools"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
)

var sess *session.Session
var bucket *string

var FailedToUploadError = fmt.Errorf("failed to upload file")
var FailedToDownloadError = fmt.Errorf("failed to download file")
var EmptyDownloadResponse = fmt.Errorf("downloaded file was empty")

type AwsS3Storage struct {
}

func (*AwsS3Storage) Set(key string, value []byte) error {
	_, err := Upload(key, value)
	if err != nil {
		return err
	}

	return nil
}

func (*AwsS3Storage) Get(key string) ([]byte, error) {
	response, err := Download(key)
	if err != nil {
		return nil, err
	}
	return response, nil
}

// Upload uploads bytes to s3bucket bucket under remoteFilename key and throws error if upload fails
func Upload(remoteFilename string, content []byte) (*s3manager.UploadOutput, error) {

	uploader := s3manager.NewUploader(GetSession())
	output, err := uploader.Upload(&s3manager.UploadInput{
		Bucket: aws.String(*GetBucket()),
		Key:    aws.String(remoteFilename),
		Body:   bytes.NewReader(content),
	})
	if err != nil {
		return nil, err
	}

	return output, nil
}

// Download downloads file from s3bucket bucket and throws error when download fails or response is empty
func Download(remoteFilename string) ([]byte, error) {
	downloader := s3manager.NewDownloader(GetSession())
	buf := aws.NewWriteAtBuffer([]byte{})

	numBytes, err := downloader.Download(buf,
		&s3.GetObjectInput{
			Bucket: aws.String(*GetBucket()),
			Key:    aws.String(remoteFilename),
		})

	if err != nil {
		return nil, FailedToDownloadError
	}
	if numBytes == 0 {
		return nil, EmptyDownloadResponse
	}

	return buf.Bytes(), nil
}

func GetSession() *session.Session {
	if sess == nil {
		sess = createSession()
	}
	return sess
}

func createSession() *session.Session {
	s, err := session.NewSession()
	if err != nil {
		fmt.Printf("ERROR: failed to create session to AWS S3. %sess\n", err)
		os.Exit(1)
	}
	return s
}

func GetBucket() *string {
	if bucket == nil {
		tmp := tools.Env("AWS_S3_CDN_BUCKET_NAME", "pl-front-cdn")
		bucket = &tmp
	}
	return bucket
}
