package http

import (
	"net/http"

	"bitbucket.org/limango/newsletter_api/service"
)

type ServerConfig struct {
	Port string
}

func NewServer(service *service.NewsletterService, config *ServerConfig) *http.Server {

	handler := NewHandler(service)
	routesHandler := NewRouter(handler)

	return &http.Server{
		Addr:    config.Port,
		Handler: routesHandler,
	}
}
