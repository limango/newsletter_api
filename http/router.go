package http

import (
	"net/http"

	"github.com/gorilla/mux"
	"github.com/swaggo/http-swagger"
)

func NewRouter(handler *Handler) *mux.Router {
	router := mux.NewRouter()

	router.Methods("GET").Path("/newsletter/{id:[0-9]+}").HandlerFunc(handler.NewsletterGetApiHandler)
	router.Methods("POST").Path("/newsletter/json").HandlerFunc(handler.NewsletterPostJsonHandler)
	router.Methods("GET").Path("/").HandlerFunc(handler.NewsletterPostJsonHandler)
	serveSwagger(router)

	return router
}

func serveSwagger(router *mux.Router) {
	router.Methods("GET").Path("/").HandlerFunc(http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		http.Redirect(res, req, "/swagger/index.html", 302)
	}))
	router.Methods("GET", "POST").PathPrefix("/swagger/").HandlerFunc(httpSwagger.WrapHandler)
}
