package http

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"bitbucket.org/limango/newsletter_api/logger"

	"bitbucket.org/limango/newsletter_api/service"
	"github.com/gorilla/mux"
)

type Handler struct {
	service service.NewsletterService
}

func NewHandler(s *service.NewsletterService) *Handler {
	return &Handler{
		*s,
	}
}

// @Summary Get newsletter by id
// @Param newsletter_id path string false "newsletter id"
// @Success 200 "newsletter json"
// @Failure 400 "bad request"
// @Router /newsletter/{id:[0-9]+}} [GET]
func (handler Handler) NewsletterGetApiHandler(rw http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id := vars["id"]

	value, err := handler.service.GetNewsletter(id)
	if err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		return
	}

	rw.Write([]byte(value))
}

// @Summary Post newsletter json with specified id
// @Param newsletter_id path string false "newsletter id"
// @Success 200 "OK"
// @Failure 400 "Bad request"
// @Router /newsletter/json [POST]
func (handler Handler) NewsletterPostJsonHandler(rw http.ResponseWriter, req *http.Request) {
	body, _ := ioutil.ReadAll(req.Body)
	go handler.saveNewsletter(body)
	rw.WriteHeader(http.StatusOK)
}

func (handler Handler) saveNewsletter(body []byte) {
	nt, err := handler.service.SaveNewsletter(body)
	if err != nil {
		logger.Log(err.Error(), logger.Error)
		return
	}
	logger.Log(fmt.Sprintf("Successfully saved newsletter group_id = %d", nt.GroupId), logger.Success)
}
