ARG SERVICE_NAME="newsletter_api"
ARG PACKAGE="newsletter_api"
ARG LISTEN=":80"

# Start from a Debian image with the latest version of Go installed
# and a workspace (GOPATH) configured at /go.
FROM golang

# set up build args
ARG SERVICE_NAME
ARG PACKAGE
ARG LISTEN

#disable crosscompiling
ENV CGO_ENABLED=0

#compile linux only
ENV GOOS=linux

#get packages
RUN go get github.com/swaggo/swag/cmd/swag

WORKDIR /go/src/${PACKAGE}

# Copy the local package files to the container's workspace.
ADD . /go/src/${PACKAGE}
RUN go get .

#build the binary
#RUN go get .
RUN swag init .
RUN go build -a -o /go/bin/${SERVICE_NAME}

# STAGE 2
# Minimal build with /bin/sh support and wget for healthcheck
FROM alpine

# set up build and runtime args
ARG SERVICE_NAME
ARG PACKAGE
ARG LISTEN

ENV SERVICE_NAME=${SERVICE_NAME}
ENV PACKAGE=${PACKAGE}
ENV LISTEN=${LISTEN}

WORKDIR /opt/

# Copy the local package files to the container's workspace.
COPY --from=0 /go/bin/${SERVICE_NAME} .
COPY --from=0 /usr/share/zoneinfo/ /usr/share/zoneinfo
COPY --from=0 /etc/ssl/certs/ /etc/ssl/certs/

HEALTHCHECK --timeout=1s \
  CMD wget --quiet --tries=1 --spider http://${LISTEN}/healthcheck || exit 1

CMD ["sh", "-c", "/opt/${SERVICE_NAME}"]
